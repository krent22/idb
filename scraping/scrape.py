# -*- coding: UTF-8-*-

import requests
from bs4 import BeautifulSoup
import re
import codecs
import pandas as pd
import json
import os


HARVARD_API_KEY = "d3b00319-10ed-4b9c-9f37-372a21fd0cf4"

chicago_url = "https://api.artic.edu/api/v1"
met_url = "https://collectionapi.metmuseum.org/public/collection/v1"
harvard_url = "https://api.harvardartmuseums.org"
wiki_url = "https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=text&page=List_of_most-visited_art_museums"
base_wiki = "https://en.wikipedia.org"
wikidata_base_link = "https://www.wikidata.org/w/api.php"

artist_fields = {
    'tags': ['name', 'birth_date', 'death_date', 'description', 'aliases'],
    'file_name': 'artists'
}
artwork_fields = {
    'tags': ['id', 'name', 'date_of_creation', 'country', 'medium', 'on_display', 'artist_name', 'artist_gender', 'category', 'type', 'dimensions', 'image'],
    'file_name': 'artworks'
}
museum_fields = {
    'tags': ['id', 'name', 'country', 'visit_stats', 'visit_count', 'date_established', 'address', 'director', 'collection_size', 'website', 'logo', 'image', 'map'],
    'file_name': 'museums'
}

# -----------------  NOTES  -----------------
#
# - The Harvard API seems to have the best information about artists
# - The Met API seems to have the best information about artworks
# - The Chicago API has good information on artworks, but poor information about artists.
#   I'm unsure whether I should integrate information from this API.
#
# -------------------------------------------


# Function to write contents of a data scrape to csv file
# data: list of dictionaries (one per instance)
def write_to_csv(data, fields):
    with codecs.open(fields['file_name'] + '.csv', "a", 'utf-8') as file:
        file.write(','.join(fields['tags']) + '\n')
        for i in data:
            rst = []
            for k in fields['tags']:
                if not k in i or not i[k]:
                    rst.append('NULL')
                else:
                    rst.append(
                        '\"' + str(i[k]) + '\"' if ',' in str(i[k]) else str(i[k]))
            file.write(','.join(rst) + '\n')
    file.close()


# -----------------  ARTISTS DATA SCRAPING  -----------------

# TODO: Integrate data from multiple sources (Met, Harvard, etc.)
# TODO: Artist gender
# TODO: Artist country of birth
# TODO: Artist status
# TODO: Artist ethnicity
# TODO: Number of works
# TODO: (Possibly) get description from Wikipedia if missing

# Chicago API scraper
def get_artists(limit=None):
    querey_params = '/agents/search?query[term][is_artist]=true&limit=' + str(
        limit) if limit else '/agents/search?query[term][is_artist]=true'
    response = requests.get(chicago_url + querey_params).json()
    result = []
    for i in range(len(response['data'])):
        data = {'id': i}
        data['name'] = response['data'][i]['title']
        info = requests.get(chicago_url + '/agents/' +
                            str(response['data'][i]['id'])).json()['data']
        data['birth_date'] = info['birth_date']
        data['death_date'] = info['death_date']
        data['description'] = info['description']
        data['aliases'] = info['alt_titles']
        result.append(data)
    return result

# Harvard API scraper


def get_harvard_artists(limit=None):
    querey_params = '/person?apikey=' + HARVARD_API_KEY
    if limit:
        querey_params = querey_params + '&size=' + str(limit)
    response = requests.get(harvard_url + querey_params).json()
    result = []
    for i in range(len(response['records'])):
        data = {'id': i}

        data['name'] = response['records'][i]['displayname']
        if response['records'][i]['gender'] and response['records'][i]['gender'] != 'unknown':
            data['gender'] = response['records'][i]['gender']
        if response['records'][i]['datebegin']:
            data['birth_date'] = response['records'][i]['datebegin']
        if response['records'][i]['dateend']:
            data['death_date'] = response['records'][i]['dateend']
        if response['records'][i]['birthplace']:
            data['country_of_birth'] = response['records'][i]['birthplace']
        if response['records'][i]['culture']:
            data['ethnicity'] = response['records'][i]['culture']
        if 'names' in response['records'][i]:
            data['aliases'] = '\"' + ','.join(n['displayName']
                                              for n in response['records'][i]['names']) + '\"'
        result.append(data)
    return result

# Function used to generate csv file


def write_artists(limit=None):
    print("Getting artist data ...")
    data = get_artists(limit)
    print("Writing to csv ...")
    write_to_csv(data, artist_fields)
    print('done')


# -----------------  ARTWORKS DATA SCRAPING  -----------------

# TODO: Integrate data from multiple sources (Met, Harvard, etc.)
# TODO: Add foreign keys (artist_id, museum_id) to get artist gender, museum, and artwork location
#       [Can only be done once we have artist and museum tables; probably need to add columns using SQL]
# TODO: Fix issue where API refuses limit past 100

def get_artworks(limit=None):
    querey_params = '/artworks?limit=' + str(limit) if limit else '/artworks'
    response = requests.get(chicago_url + querey_params).json()
    result = []
    for i in range(len(response['data'])):
        data = {'id': i}
        data['name'] = response['data'][i]['title']
        data['date_of_creation'] = response['data'][i]['date_start']
        data['country'] = response['data'][i]['place_of_origin']
        data['medium'] = response['data'][i]['medium_display']
        data['on_display'] = response['data'][i]['is_on_view']
        data['artist_name'] = response['data'][i]['artist_title']

        # GET MET DATA HERE
        category_candidates = [
            response['data'][i]['style_title'],
            *response['data'][i]['category_titles'],
            *response['data'][i]['term_titles']
        ]
        data['category'] = next(
            (i for i in category_candidates if i is not None), None)
        data['type'] = response['data'][i]['artwork_type_title'].capitalize()
        data['dimensions'] = response['data'][i]['dimensions']
        result.append(data)
    return result

# Met API scraper


def get_met_artwork(id):
    resp = requests.get(met_url + '/objects/' + id).json()
    data = {}
    if resp['primaryImage']:
        data['image'] = resp['primaryImage']
    # MORE IMAGES:  resp['additionalImages']
    if resp['classification']:
        data['type'] = resp['classification']
    if resp['artistDisplayName']:
        data['artist'] = resp['artistDisplayName']
    if resp['artistNationality']:
        data['artist_nationality'] = resp['artistNationality']
    if resp['artistGender']:
        data['artist_gender'] = resp['artistGender']
    if resp['repository']:
        data['museum'] = resp['repository']
    if resp['culture']:
        data['culture'] = resp['culture']
    if resp['medium']:
        data['medium'] = resp['medium']
    location_result = []
    if resp['city']:
        location_result += resp['city']
    if resp['state']:
        location_result += resp['state']
    if resp['country']:
        location_result += resp['country']
    location_result = ','.join(location_result)
    if location_result:
        data['location'] = location_result
    if resp['dimensions']:
        data['dimensions'] = resp['dimensions']

# Attempt to retrieve more information about this artwork from the MET API
# def get_met_artwork_from_chicago_id(chic_id):
#     chic_resp = requests.get(chicago_url + '/artworks/' + str(chic_id)).json()
#     met_resp = requests.get(met_url + '/search?title=true&dateBegin=' + str(chic_resp['data']['date_start']) + '&dateEnd=' +
#                             str(chic_resp['data']['date_end']) + '&q=' + '\"' + chic_resp['data']['title'] + '\"').json()
#     # If successful, assume the first returned ID refers to the same artwork
#     return get_met_artwork(met_resp['objectIDs'][0]) if met_resp['objectIDs'] else None

# print(get_met_artwork_from_chicago_id(29669))

# Function used to generate csv file


def write_artworks(limit=None):
    print("Getting artwork data ...")
    data = get_artworks(limit)
    print("Writing to csv ...")
    write_to_csv(data, artwork_fields)
    print('done')


# -----------------  MUSEUMS DATA SCRAPING  -----------------

# TODO:  Remove substrings of the form '[x]'
# TODO:  Add 2 text attributes

# Wikidata scraper
def get_museums(limit=None):
    response = requests.get(wiki_url).json()
    soup = BeautifulSoup(response['parse']['text']['*'], "html.parser")

    result = []
    body = soup.tbody.find_all('tr')
    if not limit:
        limit = len(body)
    for i in range(limit):
        result.append(get_museum_info(body, i))

    return result[1:]


def get_museum_info(body, id):
    data = body[id]
    td_data = data.find_all('td')
    if td_data:
        name = list(td_data[1].strings)[0]
        if len(name) == 1:
            name = name[0]
        location = td_data[2].find('a').attrs['title']
        visit_stats = list(td_data[3].strings)[0]
        visit_count = visit_stats.lstrip().split(' ')[0].replace(',', '')
        # PICTURES:  td_data[4]
        result = {'id': str(id), 'name': name, 'country': location,
                  'visit_stats': visit_stats, 'visit_count': visit_count}
        href = data.find('a').attrs['href']
        # Get info from the wiki page for this specific museum
        if href:
            href_data = get_museum_wiki_page_info(href)
            for k, v in href_data.items():
                result[k] = v

        return result


def get_museum_wiki_page_info(href):
    response = requests.get(base_wiki + href)
    soup = BeautifulSoup(response.text, "html.parser")
    data = soup.tbody.find_all('tr')

    result = {}

    # Images Info   NOTE: I'm assuming the images come in order of LOGO, IMAGE, MAP; but this might be inconsisitent
    images = [d.td.a.img.attrs['src']
              for d in data if d.find('td', class_='infobox-image') != None]
    if len(images) > 2:
        result['logo'] = images[0]
        result['image'] = images[1]
        result['map'] = images[2]
    elif len(images) == 2:
        result['image'] = images[0]
        result['map'] = images[1]
    elif images:
        result['image'] = images[0]

    # Text attributes info
    labels_info = {d.th.string: list(d.td.strings) for d in data if d.find(
        'th', class_='infobox-label') != None}
    if 'Established' in labels_info:
        result['date_established'] = labels_info['Established'][0]
    if 'Location' in labels_info:
        result['address'] = ' '.join(
            labels_info['Location']).replace(' ,', ',')
        result['address'] = re.sub(" +", ' ', result['address'])
    if 'Director' in labels_info:
        result['director'] = labels_info['Director'][0]
    if 'Collection size' in labels_info:
        fil = filter(lambda s: s[0] != '[' and s[-1] !=
                     ']', labels_info['Collection size'])
        result['collection_size'] = ' '.join(fil)
    if 'Website' in labels_info:
        result['website'] = "".join(labels_info['Website'])

    return result

# Function used to generate csv file


def write_museums(limit=None):
    print("Getting museum data ...")
    data = get_museums(limit)
    write_to_csv(data, museum_fields)
    print('done')


Wikidata_fields_dict = {
    'art_museum': "Q207694",
    'instance_of': "P31",
    'logo': "P154",
    'image_exterior': "P18",
    'image_interior': "P5775",
    'date_established': "P571",
    'founder': "P112",
    'director': "P1037",
    'country': "P17",
    'collection_size': "P1436",
    'visitors_per_year': "P1174",
    'phone_number': "P1329",
    'website': "P856",
    'occupation': "P106",
    'image': "P18"
}

base_wiki_image = "https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/"


def check_is_art_museum(result):
    if result:
        instance_of_attr = get_field_info(result, 'instance_of')
        if instance_of_attr:
            for i in instance_of_attr:
                if i['mainsnak']['datavalue']['value']['id'] == Wikidata_fields_dict['art_museum']:
                    return True
    return False


def get_field_info(result, field, get_value=False):
    if result:
        result = result['entities'][list(result['entities'].keys())[0]]['claims']
        field = Wikidata_fields_dict[field]
        if field in result and result[field]:
            if get_value:
                return result[field][0]['mainsnak']['datavalue']['value']
            else:
                return result[field]


def extract_name(result):
    return result['entities'][list(result['entities'].keys())[0]]['labels']['en']['value']

# -------------  IMAGES (Might need a better link)  -------------- #


def extract_logo(result):
    rst = get_field_info(result, 'logo', get_value=True)
    if rst:
        return base_wiki_image + rst.replace(' ', '_')


def extract_image_exterior(result):
    rst = get_field_info(result, 'image_exterior', get_value=True)
    if rst:
        return base_wiki_image + rst.replace(' ', '_')


def extract_image_interior(result):
    rst = get_field_info(result, 'image_interior', get_value=True)
    if rst:
        return base_wiki_image + rst.replace(' ', '_')
# ---------------------------------------------------------------- #


def extract_date_established(result):
    rst = get_field_info(result, 'date_established', get_value=True)
    if rst:
        return int(rst['time'][1:5])


def extract_founder(result):
    founder_attr = get_field_info(result, 'founder', get_value=True)
    if founder_attr:
        entity_id = founder_attr['id']
        querey_params = {
            'action': 'wbgetentities',
            'ids': entity_id,
            'format': 'json',
            'language': 'en'
        }
        entity_result = requests.get(wikidata_base_link, querey_params).json()
        return extract_name(entity_result)


def extract_director(result):
    director_attr = get_field_info(result, 'director', True)
    if director_attr:
        entity_id = director_attr['id']
        querey_params = {
            'action': 'wbgetentities',
            'ids': entity_id,
            'format': 'json',
            'language': 'en'
        }
        entity_result = requests.get(wikidata_base_link, querey_params).json()
        return extract_name(entity_result)


def extract_country(result):
    country_attr = get_field_info(result, 'country', True)
    if country_attr:
        entity_id = country_attr['id']
        querey_params = {
            'action': 'wbgetentities',
            'ids': entity_id,
            'format': 'json',
            'language': 'en'
        }
        entity_result = requests.get(wikidata_base_link, querey_params).json()
        return extract_name(entity_result)


def extract_collection_size(result):
    rst = get_field_info(result, 'collection_size', get_value=True)
    if rst:
        return int(rst['amount'][1:])


def extract_visitors_per_year(result):
    rst = get_field_info(result, 'visitors_per_year', get_value=True)
    if rst:
        return int(rst['amount'][1:])


def extract_phone_number(result):
    return get_field_info(result, 'phone_number', get_value=True)


def extract_website(result):
    return get_field_info(result, 'website', get_value=True)

# Parent scraping function


def scrape_from_museum_name(records, name):
    search_params = {
        'action': 'wbsearchentities',
        'search': name,
        'format': 'json',
        'language': 'en',
    }

    # Find the first Wiki item with 'art museum' as its 'instance of' field
    for item in requests.get(wikidata_base_link, params=search_params).json()['search']:
        querey_params = {
            'action': 'wbgetentities',
            'ids': item['id'],
            'format': 'json',
            'language': 'en'
        }
        # check if id is already present
        if querey_params['ids'] not in records:
            result = requests.get(wikidata_base_link,
                                  params=querey_params).json()

            if check_is_art_museum(result):
                data = {}
                data['name'] = extract_name(result)
                data['logo'] = extract_logo(result)
                data['image_exterior'] = extract_image_exterior(result)
                data['image_interior'] = extract_image_interior(result)
                data['date_established'] = extract_date_established(result)
                data['founder'] = extract_founder(result)
                data['director'] = extract_director(result)
                data['country'] = extract_country(result)
                data['collection_size'] = extract_collection_size(result)
                data['visitors_per_year'] = extract_visitors_per_year(result)
                data['phone_number'] = extract_phone_number(result)
                data['website'] = extract_website(result)
                records[item['id']] = data
                return None
                # return data
    print("Failed to find museum with search querey:  " + name)
    return name


def write_museums():
    museums = pd.read_json('museums.json')
    records = {}
    missing = []
    for name in museums[0]:
        print("Getting info for: ", name)
        rst = scrape_from_museum_name(records, name)
        if rst:
            missing.append({'name': rst})
    # write records to csv
    new_museum_fields = {
        'tags': ['name', 'country', 'visitors_per_year', 'visit_count', 'date_established', 'founder', 'director', 'collection_size', 'phone_number', 'website', 'logo', 'image_exterior', 'image_interior'],
        'file_name': 'museums_api'
    }
    if missing:
        write_to_csv(missing, {'tags': ['name'], 'file_name': 'missing'})
    write_to_csv(list(records.values()), new_museum_fields)


# Code used to filter valid artworks into json
def write_artworks():
    art_data = pd.read_json('source_files/all_artworks.json')
    artist_data = pd.read_json('parsed_files/valid_artists.json')
    museum_data = pd.read_json('parsed_files/valid_museums.json')
    artworks = {}

    id = 0
    for i in art_data['data']:
        artist = 0
        owning_museum = 0
        # artwork is valid if it has an existing exhibition history
        if ('exhibition-history' in i) and i['exhibition-history'] is not None:
            artworks[id] = i
            artist = artworks[id]['artist-name']
            owning_museum = artworks[id]['owning-museum']
            
        # add artist id matching this artwork
        for artist_id in artist_data:
            if artist_data[artist_id]['name'] == artist:
                artworks[id]['artist_id'] = artist_id

        # find owning museum id matching this artwork
        for museum_id in museum_data:
            if museum_data[museum_id]['name'] == owning_museum:
                artworks[id]['owning_museum_id'] = museum_id

        id += 1

    with open ('valid_artworks.json', 'w') as jsonfile:
        json.dump(artworks, jsonfile, indent = 4)


Wikidata_is_artist_ids = {
    'artist': "Q483501",
    'painter': "Q1028181",
    'graphic_artist': "Q1925963",
    'sculptor': "Q1281618",
    'drawer': "Q15296811"
}
def check_if_artist(result):
    if result:
        instance_of = get_field_info(result, 'instance_of')
        if instance_of:
            for i in instance_of:
                if i['mainsnak']['datavalue']['value']['id'] in Wikidata_is_artist_ids.values():
                    return True
        occupation = get_field_info(result, 'occupation')
        if occupation:
            for i in occupation:
                if i['mainsnak']['datavalue']['value']['id'] in Wikidata_is_artist_ids.values():
                    return True

def scrape_artist_image(artist_name):
    search_params = {
        'action': 'wbsearchentities',
        'search': artist_name,
        'format': 'json',
        'language': 'en',
    }
    for item in requests.get(wikidata_base_link, params=search_params).json()['search']:
        querey_params = {
            'action': 'wbgetentities',
            'ids': item['id'],
            'format': 'json',
            'language': 'en'
        }
        result = requests.get(wikidata_base_link, params=querey_params).json()
        if check_if_artist(result):
            image = get_field_info(result, 'image', get_value=True)
            if image:
                return base_wiki_image + image.replace(' ', '_')
            return None

def write_artists_json_w_image(name='new_valid_artists.json'):
    artists_data = pd.read_json('csvs/valid_artists.json', orient="index")
    artists_data['death_date'] = artists_data['death_date'].astype('Int64')  # apparently, Pandas assumes the datatype should be float64. must undo this conversion.
    artists_data['image'] = artists_data['name'].map(scrape_artist_image)
    artists_data.to_json(os.getcwd() + '/' + 'csvs/' + name, orient='index', indent=4)
