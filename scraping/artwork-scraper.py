# code adapted from cleveland open access documentation: https://openaccess-api.clevelandart.org/#appendix-d
# add to dockerfile: pip install parse
# https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html for converting to dataframe

# scrape artwork data from both harvard and cleveland apis
import pandas as pd
from parse import *
import requests

museums = set()

def get_cleveland_info(id, artist_name, artist_gender):
    global museums

    url = "https://openaccess-api.clevelandart.org/api/artworks/" + str(id)
    r = requests.get(url)
    data = r.json()['data']

    work = {}
    work['name'] = data['title']
    work['country-of-origin'] = data['culture'][0]
    work['date-of-creation'] = data['creation_date']
    work['category'] = data['department']
    work['type'] = data['type']
    work['artist-name'] = artist_name
    work['artist-gender'] = artist_gender
    work['owning-museum'] = "Cleveland Museum of Art"
    
    if data['current_location']:
        work['location'] = data['current_location'] + ", " + work['owning-museum']
    else:
        work['location'] = "Unknown"
    
    work['dimensions'] = data['measurements']
    work['acquisition'] = data['creditline']
    work['medium'] = data['technique']
    work['description'] = data['wall_description']

    exhibition_history = []
    try:
        exhibitions = data['exhibitions']['current']
        for ex in exhibitions:
            try:
                _, loc, _, _, dates, _ = parse("<i>{}</i>. {}, {}, {} (organizer) ({}){}", ex['description'])
                exhibition_history.append({"location": loc, "Dates": dates})
                museums.add(loc)
            except:
                pass
        if exhibition_history:
            work['exhibition-history'] = exhibition_history
        else:
            work['exhibiton-history'] = None
    except:
        work['exhibition-history'] = None

    try:
        work['image-url'] = data['images']['web']['url']
    except:
        work['image-url'] = None

    return work

def get_harvard_info(id, artist_name, artist_gender):
    global museums
    
    url = "https://api.harvardartmuseums.org/object/" + str(id) + "?apikey=d3b00319-10ed-4b9c-9f37-372a21fd0cf4"
    r = requests.get(url)
    data = r.json()

    work = {}
    work['name'] = data['title']
    work['country-of-origin'] = data['culture']
    work['date-of-creation'] = data['dated']
    work['category'] = data['division']
    work['type'] = data['technique']
    work['artist-name'] = artist_name
    work['artist-gender'] = artist_gender
    work['owning-museum'] = "Harvard Art Museum"
    
    try:
        work['location'] = "Gallery " + data['gallery']['gallerynumber'] + ", " + work['owning-museum']
    except:
        work['location'] = "Unknown"
    
    work['dimensions'] = data['dimensions']
    work['acquisition'] = data['creditline']
    work['medium'] = data['medium']
    work['description'] = data['description']

    exhibition_history = []
    try:
        exhibitions = data['exhibitions']
        for ex in exhibitions:
            try:
                _, loc, _ = parse("<em>{}</em>, {}, {}", ex['citation'])
                dates = ex['begindate'] + " - " + ex['enddate']
                exhibition_history.append({"location": loc, "Dates": dates})
                museums.add(loc)
            except:
                pass
        if exhibition_history:
            work['exhibition-history'] = exhibition_history
        else:
            work['exhibiton-history'] = None
    except:
        work['exhibition-history'] = None

    try:
        work['image-url'] = data['primaryimageurl']
    except:
        work['image-url'] = None

    return work

if __name__ == '__main__':
    
    artists = pd.read_json('all_artists.json')['data']

    all_works = {}
    artwork_list = []

    for artist in artists:
        works_list = artist['works']
        for work in works_list:
            print("Finding", work['name'])
            try:
                if 'id' in work:
                    artwork_list.append(get_cleveland_info(work['id'], artist['name'], artist['gender']))
                elif 'harvard_id' in work:
                    print("Getting harvard info")
                    artwork_list.append(get_harvard_info(work['harvard_id'], artist['name'], artist['gender']))
            except:
                pass

    all_works['data'] = artwork_list

    museum_set = pd.DataFrame(museums)
    museum_set.to_json('museums.json')

    df = pd.DataFrame(all_works)
    df.to_json('all_artworks.json')

