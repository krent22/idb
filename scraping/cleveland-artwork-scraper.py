# code from cleveland open access documentation: https://openaccess-api.clevelandart.org/#appendix-d
# add to dockerfile: pip install parse
# pip install python-dateutil
# https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html for converting to dataframe
import json
import pandas as pd
from parse import *
from datetime import *
from dateutil import parser as date_parser
import requests

artwork_list = []

def openaccess_results(params):
    url = "https://openaccess-api.clevelandart.org/api/artworks"
    r = requests.get(url, params=params)
    data = r.json()

    NOW = datetime.now()

    for artwork in data['data']:
        work = {}
        work['owning_museum'] = "Cleveland Institute of Arts"
        work['name'] = artwork['title']
        work['acquisition'] = artwork['creditline']
        work['date_of_creation'] = artwork['creation_date_latest']
        work['medium'] = artwork['technique']
        work['type'] = artwork['type']
        work['dimensions'] = artwork['measurements']
        work['location'] = artwork['current_location']
        work['category'] = artwork['department']

        try:
            artist_facts = artwork['creators'][0]['description']
            artist_name, artist_ethnicity, artist_birth, artist_death = parse("{} ({}, {}–{})", artist_facts)
            work['artist_name'] = artist_name
        except:
            work['artist_name'] = None

        try:
            work['year_of_creation'] = int(artwork['creation_date_latest'])
        except:
            work['year_of_creation'] = None
        
        try:
            work['image_link'] = artwork['images']['web']['url']
        except:
            work['image_link'] = None

        try:
            origin = artwork['culture'][0]
            country, _ = parse("{}, {}", origin)
            work['country'] = country
        except:
            work['country'] = None

        try:
            exhibitions = artwork['exhibitions']['current']
            for ex in exhibitions:
                try:
                    exhibition, ex_loc, _, _, _, end_date, _ = parse("<i>{}</i>. {}, {}, {} (organizer) ({}-{}){}", ex['description'])
                    if(date_parser.parse(ex['opening_date']) <= NOW and date_parser.parse(end_date) >= NOW):
                        work['location'] = ex_loc
                except:
                    try:
                        if(date_parser.parse(ex['opening_date']) <= NOW and date_parser.parse(end_date) >= NOW):
                            exhibition, ex_loc, _, end_date, _ = parse("<i>{}</i>. {} ({}-{}){}", ex['description'])
                            work['location'] = ex_loc
                    except:
                        pass
        except:
            pass
        
        global artwork_list
        artwork_list.append(work)

if __name__ == '__main__':
    loan_params = {
        'skip': 0,
        'limit': 100,
        'has_image': 1,
        'currently_on_loan': ""
    }

    view_params = {
        'skip': 0,
        'limit': 300,
        'has_image': 1,
        'currently_on_view': ""
    }

    openaccess_results(loan_params)
    openaccess_results(view_params)

    df = pd.DataFrame(artwork_list)
    df.to_csv('cleveland_artwork_list.csv', mode='w', index=False)  