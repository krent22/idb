# code adapted from cleveland open access documentation: https://openaccess-api.clevelandart.org/#appendix-d
# add to dockerfile: pip install parse
# https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html for converting to dataframe

# scrape a list of artists that have biographies from the cleveland api
import pandas as pd
from parse import *
import requests

artist_with_bio = set()
artist_no_bio = set()

def create_artist_sets(params):
    global artist_with_bio
    global artist_no_bio

    url = "https://openaccess-api.clevelandart.org/api/artworks"
    r = requests.get(url, params=params)
    data = r.json()

    for artwork in data['data']:        
        if artwork['exhibitions'] and artwork['exhibitions']['current']:
            # we're only interested in the artists of artworks that have an exhibition history
            creator_info = artwork['creators']
            if creator_info:
                artist_facts = artist_facts = artwork['creators'][0]['description']
                try:
                    artist_name, _ = parse("{} ({})", artist_facts)
                    artist_id = creator_info[0]['id']
                    artist = (artist_id, artist_name)
                    if creator_info[0]['biography']:
                        artist_with_bio.add(artist)
                    else:
                        artist_no_bio.add(artist)
                except:
                    pass
                
def format_into_dict(artist_set):
    artist_dict = {}
    artist_list = []
    for id, name in artist_set:
        artist_list.append({'cleveland_id': id, 'name': name})
    artist_dict['data'] = artist_list
    return artist_dict

if __name__ == '__main__':
    loan_params = {
        'skip': 0,
        'limit': 100,
        'has_image': 1,
        'currently_on_loan': ""
    }
    create_artist_sets(loan_params)

    skip = 0
    while(len(artist_with_bio) < 120):
        view_params = {
            'skip': skip,
            'has_image': 1,
            'limit': 100,
            'currently_on_view': ""
        }
        print("skip = ", skip)
        print("artists with bio: ", len(artist_with_bio))
        print("artists no bio: ", len(artist_no_bio))
        skip += 100
        create_artist_sets(view_params)

    
    create_artist_sets(view_params)

    bio_df = pd.DataFrame(format_into_dict(artist_with_bio))
    bio_df.to_json('artist_with_bio.json')  

    no_bio_df = pd.DataFrame(format_into_dict(artist_no_bio))
    no_bio_df.to_json('artist_no_bio.json')  
