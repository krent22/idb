# code adapted from cleveland open access documentation: https://openaccess-api.clevelandart.org/#appendix-d
# add to dockerfile: pip install parse
# https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html for converting to dataframe

# scrape artwork data from both harvard and cleveland apis
import pandas as pd
from parse import *
import requests

def get_museum_id(name):    
    url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" \
    + name + "&inputtype=textquery&key=AIzaSyCBufB9i-NiKC-pL9hP3-uGXLayYNro9_E"
    r = requests.get(url)
    id = None
    try:
        id = r.json()['candidates'][0]['place_id']
    except:
        pass
    return id

def output_ids():
    data = []
    museums = pd.read_json('museums.json')[0]
    for i in range(0, 303):
        try:
            data.append({museums[i]: get_museum_id(museums[i])})
        except:
            pass
    print(data)

    results = {}
    results['data'] = data

    result_df = pd.DataFrame(results)
    result_df.to_json('google-museums-results.json')


def get_museum_info(id):
    url = "https://maps.googleapis.com/maps/api/place/details/json?" \
        + "fields=business_status,formatted_address,formatted_phone_number," \
        + "geometry,icon,international_phone_number,name,opening_hours,rating," \
        + "types,website,utc_offset" \
        + "&place_id=" + id + "&key=AIzaSyCBufB9i-NiKC-pL9hP3-uGXLayYNro9_E"
    r = requests.get(url)
    data = r.json()['result']
    info = {}
    try:
        info['name'] = data['name']
    except:
        info['name'] = None
    try:
        info['business_status'] = data['business_status']
    except:
        info['business_status'] = None

    try:
        info['formatted_phone_number'] = data['formatted_phone_number']
    except:
        info['formatted_phone_number'] = None

    try:
        info['formatted_address'] = data['formatted_address']
    except:
        info['formatted_address'] = None

    try:
        info['latitude'] = data['geometry']['location']['lat']
    except:
        info['latitude'] = None

    try:
        info['longitude'] = data['geometry']['location']['lng']
    except:
        info['longitude'] = None

    try:
        info['icon'] = data['icon']
    except:
        info['icon'] = None

    try:
        info['international_phone_number'] = data['international_phone_number']
    except:
        info['international_phone_number'] = None

    try:
        info['open_hours'] = data['opening_hours']['weekday_text']
    except:
        info['open_hours'] = None
    
    try:
        info['rating'] = data['rating']
    except:
        info['rating'] = None

    try:
        info['types'] = data['types']
    except:
        info['types'] = None

    try:
        info['utc_offset'] = data['utc_offset']
    except:
        info['utc_offset'] = None

    try:
        info['website'] = data['website']
    except:
        info['website'] = None
    
    return info

def output_info():
    data = []
    museum_ids = pd.read_json('google-museum-ids.json')['data']
    for i in range(290, 303):
        try:
            single_result = {}
            single_result['search_target'] = list(museum_ids[i].keys())[0]
            single_result['google_id'] = museum_ids[i][single_result['search_target']]
            single_result['info'] = get_museum_info(single_result['google_id'])
            data.append(single_result)
        except:
            pass
    
    results = {}
    results['data'] = data

if __name__ == '__main__':
    

    # output_ids()

    output_info()


# https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=Cleveland%20museum%20of%20art&inputtype=textquery&key=AIzaSyCBufB9i-NiKC-pL9hP3-uGXLayYNro9_E

# https://maps.googleapis.com/maps/api/place/details/json?place_id=ChIJNaqG8kpI0IkRUv08p7B9GIY&key=AIzaSyCBufB9i-NiKC-pL9hP3-uGXLayYNro9_E

# need formatted_address, formatted_phone_number, latitude and longitude
# icon, international phone number, name, opening hours (weekday_text), rating,
# website, types, business status, timezone (utc offset)