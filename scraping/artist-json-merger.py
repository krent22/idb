# https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html for converting to dataframe
# merge data from both cleveland and harvard museums

import json
import pandas as pd
from parse import *
import requests

def merge_json(cleveland_artists, harvard_artists):
    total_artists = []
    for i in range(len(cleveland_artists)):
        if harvard_artists[i]:
            if(cleveland_artists[i]['name'] != harvard_artists[i]['name']):
                if not "Factory" in cleveland_artists[i]['name']:
                    cleveland_artists['other-names'] = None
                    cleveland_artists['birth-place'] = None
                    total_artists.append(cleveland_artists[i])

            else:
                harvard_info = harvard_artists[i]
                cleveland_info = cleveland_artists[i]
                if not cleveland_info['birth-date'] and harvard_info['birth-date']:
                    cleveland_info['birth-date'] = harvard_info['birth-date']
                if not cleveland_info['death-date'] and harvard_info['death-date']:
                    cleveland_info['death-date'] = harvard_info['death-date']
                if not cleveland_info['ethnicity'] and harvard_info['ethnicity']:
                    cleveland_info['ethnicity'] = harvard_info['ethnicity']
                cleveland_info['other-names'] = harvard_info['other-names']
                cleveland_info['birth-place'] = harvard_info['birth-place']
                cleveland_info['number-of-works'] += harvard_info['number-of-works']
                if harvard_info['works']:
                    cleveland_info['works'] += harvard_info['works']
                total_artists.append(cleveland_info)

    return total_artists

def add_missing_fields():
    all_info = pd.read_json('all_artists.json')
    artists = all_info['data']
    print(artists)

    for artist in artists:
        try:
            place = artist['birth-place']
        except:
            artist['birth-place'] = None
        try:
            names = artist['other-names']
        except:
            artist['other-names'] = None
    
    new_dict = {}
    new_dict['data'] = artists
    df = pd.DataFrame(new_dict)
    df.to_json('all_artists.json')
                
if __name__ == '__main__':
    
    cleveland_info = pd.read_json('cleveland_artist_info.json')
    harvard_info = pd.read_json('harvard_artist_info.json')
    
    cleveland_artists = cleveland_info['data']
    harvard_artists = harvard_info['data']

    merged_artists = {}
    merged_artists['data'] = merge_json(cleveland_artists, harvard_artists)

    df = pd.DataFrame(merged_artists)
    df.to_json('all_artists.json')
