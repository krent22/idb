# code from cleveland open access documentation: https://openaccess-api.clevelandart.org/#appendix-d
# add to dockerfile: pip install parse
# pip install python-dateutil
# https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html for converting to dataframe

import json
import pandas as pd
from parse import *
from datetime import *
import requests

artist_info = {}

def find_gender(bio):
    male = bio.count(" He ") + bio.count(" he ") + bio.count(" his ") + bio.count(" His ")
    female = bio.count(" She ") + bio.count(" she ") + bio.count(" her ") + bio.count(" Her ")
    if(male == 0 and female == 0):
        return "unknown"
    if(male > female):
        return 'male'
    else:
        return 'female'

# still missing birth-country, other-names
def retrieve_info(id):
    url = "https://openaccess-api.clevelandart.org/api/creators/" + str(id)
    r = requests.get(url)
    data = r.json()['data']

    info = {}
    info['name'] = data['name']
    info['gender'] = find_gender(data['biography'])
    info['biography'] = data['biography']
    artist_facts = data['description']
    
    try:
        artist_name, ethnicity, _, _ = parse("{} ({}, {}–{})", artist_facts)
        info['ethnicity'] = ethnicity
        
    except:
        artist_name, description = parse("{} ({})", artist_facts)
        info['unparsable description'] = description
    
    info['birth-date'] = data['birth_year']
    info['death-date'] = data['death_year']
    try:
        death_year = int(data['death_year'])
        info['status'] = 'deceased'
    except:
        info['status'] = 'alive'

    info['number-of-works'] = len(data['artworks'])
    artworks = []
    for work in data['artworks']:
        artworks.append({'id': work['id'], 'name': work['title']})
    info['works'] = artworks
    return info

def process(artists):
    complete = []
    for i in range(len(artists)):
        complete.append(retrieve_info(artists[i]['cleveland_id']))
        print("Finished finding info for person #", i)
    return complete

def harvard_find_info(name):
    url = "https://api.harvardartmuseums.org/person?q=displayname:" + name + "&apikey=d3b00319-10ed-4b9c-9f37-372a21fd0cf4"
    r = requests.get(url)
    try:
        data_list = r.json()['records']
        if not data_list:
            return None
    except:
        return None
    
    data = data_list[0]
    info = {}
    info['name'] = data['displayname']
    info['gender'] = data['gender']
    info['ethnicity'] = data['culture']
    info['birth-date'] = str(data['datebegin'])
    info['birth-place'] = data['birthplace']
    info['death-date'] = str(data['dateend'])
    try:
        death_year = data['dateend']
        if death_year < 2023:
            info['status'] = 'deceased'
    except:
        info['status'] = 'alive'

    info['number-of-works'] = data['objectcount']
    other_names = []
    try:
        for name in data['names']:
            other_names.append(name['displayname'])
        info['other-names'] = other_names
    except:
        info['other-names'] = None

    id = data['personid']
    info['works'] = find_artwork_list(id)

    return info

def find_artwork_list(id):
    url = "https://api.harvardartmuseums.org/object?person=" + str(id) + "&apikey=d3b00319-10ed-4b9c-9f37-372a21fd0cf4"
    print(url)
    r = requests.get(url)
    try:
        data_list = r.json()['records']
        if not data_list:
            return None
    except:
        return None
    
    print("got artwork list")
    artwork_list = []
    for work in data_list:
        work_info = {}
        work_info['harvard_id'] = work['objectid']
        work_info['name'] = work['title']
        artwork_list.append(work_info)
    return artwork_list

def process_harvard(artists):
    complete = []
    for i in range(len(artists)):
        complete.append(harvard_find_info(artists[i]['name']))
        print("Finished finding info for person #", i)
    return complete
                
if __name__ == '__main__':
    
    artists = pd.read_json('artist_with_bio.json')
    
    all_artists = {}
    all_artists['data'] = process(artists['data'])

    df = pd.DataFrame(all_artists)
    df.to_json('cleveland_artist_info.json')

    harvard_artists = {}
    harvard_artists['data'] = process_harvard(artists['data'])
    df_harvard = pd.DataFrame(harvard_artists)
    df_harvard.to_json('harvard_artist_info.json')
