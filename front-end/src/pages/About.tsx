import Button from "react-bootstrap/Button";
import "./About.css";
import Card from "react-bootstrap/Card";
import { useState, useEffect} from "react"

// Took code/inspiration from AnimalWatch
// GitLab: https://gitlab.com/JohnPowow/animalwatch

function About() {
  const gitCommit = "https://gitlab.com/api/v4/projects/39550052/repository/contributors"
  const gitIssue = "https://gitlab.com/api/v4/projects/39550052/issues?per_page=100"
  // const [isLoaded, setIsLoaded] = useState(false)
  const [commits, setCommits] = useState([])
  const [issues, setIssues] = useState([])
  // const [totalIssues, setTotalIssues] = useState([])
  // const [totalCommits, setTotalCommits] = useState([])
  const gitApi = "https://gitlab.com/api/v4/projects/39550052/repository/contributors"

  let commitData

  // manually figure id for now
  var idKaren = 6
  var idDon = 4
  var idJoel = 3
  var idJoseph = 5
  var idPolo = 1
  var totalCommits = 0
  var totalIssues = 0

  async function GitStats() {
    
    // attempt at finding id via email
    const emailKaren = "karen461907@gmail.com"
    const emailDon = "d.nguyen3235@gmail.com"
    const emailJoel = "jokerchan1st@gmail.com"
    const emailPolo = "ll34552@eid.utexas.edu"
    const emailJoseph = "jtmartin@caltech.edu"
    const response = await fetch(gitApi)
    const responseData = await response.json()

    commitData = responseData.map(function(repo: any) {
      return(
        <p key={repo.email}>{repo.commits}</p>
      )
    })
    setCommits(commitData)
  }

  useEffect(() => {
    GitStats()
  }, [])

  return (
    <div className="background-box">
      {/* outside-about-card for styling in css later. */}
      <Card className="about-us-card">
        <Card.Header as="h3">About Us</Card.Header>
        <Card.Body>
          <Card.Title>Our Mission</Card.Title>
          <Card.Text>
            <p>
              {" "}
              An art catalogue that details the art piece, facts about the
              artist/creator, and museums that art pieces are currently being
              exhibited.{" "}
            </p>

            <p>
              {" "}
              We help you get more in touch with different types of art and
              artists.{" "}
            </p>
          </Card.Text>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">GitLab Stats</Card.Header>

        <Card.Body>
          {/* TODO Variables for fields. */}
          <Card.Text>
            <p>Total Commits: {totalCommits}</p>
            <p>Total Issues: {totalIssues}</p>
            <p>Total Unit Tests: {0}</p>
          </Card.Text>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Body>
          <Card.Title as="h3">Our Team</Card.Title>
          <div className="member-flex">
            <Card className="member-card">
              <Card.Header as="h4">Polo Lopez</Card.Header>
              <Card.Body>
                {/* <Card.Title>Special title treatment</Card.Title> */}
                <Card.Text>
                  {/* TODO Variables for fields. */}
                  <p>
                    <p>
                      {" "}
                      Blah Blah is a Blah Blah studying Blah Blah at Blah Blah{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Documentation and API Design</p>
                  <b>Total Commits: {commits[idPolo]}</b> 
                  <b>Total Issues: {}</b> <br />
                  <b>Total Unit Tests: {0}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
              <Card.Header as="h4">Joseph Martinez</Card.Header>
              <Card.Body>
                {/* <Card.Title>Special title treatment</Card.Title> */}
                <Card.Text>
                  {/* TODO Variables for fields. */}
                  <p>
                    <p>
                      {" "}
                      Blah Blah is a Blah Blah studying Blah Blah at Blah Blah{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Front End</p>
                  <b>Total Commits: {commits[idJoseph]}</b> 
                  <b>Total Issues: {}</b> <br />
                  <b>Total Unit Tests: {0}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
              <img 
                src= "./images/DonPortrait.jpeg" 
                height = "250" 
                width = "225" 
                alt=""
              />
              <Card.Header as="h4">Don Nguyen</Card.Header>
              <Card.Body>
                {/* <Card.Title>Special title treatment</Card.Title> */}
                <Card.Text>
                  {/* TODO Variables for fields. */}
                  <p>
                    <p>
                      {" "}
                      I am a third-year majoring in computer science at UT Austin. I was born in Arlington, TX but spent a good part of my childhood in Kansas City, Missouri. I like to play tennis or video games in my free time.{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>API Integration and Project Manager</p>
                 
                  <b>Total Commits: {commits[idDon]}</b>
                  <b>Total Issues: {}</b> <br />
                  <b>Total Unit Tests: {0}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
              <img
                src= "./images/JoelPortrait.jpeg"
                height= "250"
                width= "225"
                alt= ""
              />
              <Card.Header as="h4">Joel Setiawan Chong</Card.Header>
              <Card.Body>
                {/* <Card.Title>Special title treatment</Card.Title> */}
                <Card.Text>
                  {/* TODO Variables for fields. */}
                  <p>
                    <p>
                      {" "}
                      I am a third-year computer science major at UT Austin. I was born and raised near Arlington, TX. I enjoy taking walks and reading books.{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Front End</p>
                  <b>Total Commits: {commits[idJoel]}</b> 
                  <b>Total Issues: {}</b> <br />
                  <b>Total Unit Tests: {0}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
              <img
                src= "./images/KarenPortrait.jpg"
                height= "250"
                width= "225"
                alt= ""
              />
              <Card.Header as="h4">Karen Tseng</Card.Header>
              <Card.Body>
                {/* <Card.Title>Special title treatment</Card.Title> */}
                <Card.Text>
                  {/* TODO Variables for fields. */}
                  <p>
                    <p>
                      {" "}
                      I am a third-year computer science major at UT Austin. I was born in California, but grew up in Katy, TX. In my free time, I love spending time with family and watching anime. {" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Front End</p>
                  <b>Total Commits: {commits[idKaren]}</b>
                  <b>Total Issues: {}</b> <br />
                  <b>Total Unit Tests: {0}</b>
                </Card.Text>
                {/* <Button variant="primary">Go somewhere</Button> */}
              </Card.Body>
            </Card>
          </div>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">Phase Leaders</Card.Header>
        <Card.Body>
          {/* TODO Variables for fields. */}
          <Card.Text>
            <p>Phase 1: Don Nguyen</p>
            <p>Phase 2: </p>
            <p>Phase 3: </p>
            <p>Phase 4: </p>
          </Card.Text>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">APIs Used</Card.Header>
        <div className="member-flex">
          <Card className="member-card">
            {/* <Card.Title as="h4">API #1: Museum of Metropolitan Art</Card.Title> */}
            <h3>
              {" "}
              <a href="https://metmuseum.github.io/">
                Museum of Metropolitan Art
              </a>{" "}
            </h3>

            <img
              src="The Metropolitan Museum of Art.png"
              height="200"
              width="300"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://api.artic.edu/docs/#introduction">
                Art Institute of Chicago
              </a>{" "}
            </h3>

            <img
              src="Front of Art Institute of Chicago.png"
              height="200"
              width="300"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://query.wikidata.org/">
                WikiData Search Query Language
              </a>{" "}
            </h3>

            <img
              src="Wikidata_stamp.png"
              height="200"
              width="250"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://gitlab.com/krent22/idb">
                Our API TODO CHANGE LINK
              </a>{" "}
            </h3>
            <Card.Text>
              <p>TODO Description</p>
            </Card.Text>
          </Card>
        </div>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">Our Toolchain</Card.Header>
        <div className="member-flex">
          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://reactjs.org/">React</a>{" "}
            </h3>

            <img
              src="logo192.png"
              height="200"
              width="200"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://react-bootstrap.github.io/">
                React Bootstrap
              </a>{" "}
            </h3>

            <img
              src="React Bootstrap.png"
              height="200"
              width="200"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://gitlab.com/">GitLab</a>{" "}
            </h3>

            <img
              src="GitLab.png"
              height="200"
              width="200"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://aws.amazon.com/">Amazon Web Services</a>{" "}
            </h3>

            <img
              src="AWS.png"
              height="180"
              width="240"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://www.namecheap.com/">Namecheap</a>{" "}
            </h3>

            <img
              src="Namecheap.png"
              height="180"
              width="280"
              alt="picture of react logo"
            />
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a href="https://www.postman.com/">Postman</a>{" "}
            </h3>

            <img
              src="Postman.jpg"
              height="180"
              width="180"
              alt="picture of react logo"
            />
          </Card>
        </div>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">Project Links</Card.Header>
        <h3>
          {" "}
          <a href="https://gitlab.com/krent22/idb">Project GitLab</a>{" "}
        </h3>

        <h3>
          {" "}
          <a href="https://www.postman.com/">
            Project Postman STILL NEED TO UPDATE LINK
          </a>{" "}
        </h3>
      </Card>
    </div>
  );
}

export default About;
