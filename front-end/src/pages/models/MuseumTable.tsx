
import { Row, Container } from 'react-bootstrap'
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useState, useMemo, useEffect } from 'react';
import { PageControl } from './../../components/ModelPagination'
import Table from 'react-bootstrap/Table';
import Spinner from 'react-bootstrap/Spinner';
import { SearchBar } from '../../components/SearchBar';
import { comma_sep_num } from '../instances/InstanceConversions'
import Highlighter from 'react-highlight-words'

import './Grid.css'
import filterData from './museumFilters.json';

interface MuseumTableProps {
  data: any
  highlight: string[]
}

export function MuseumTable(props: MuseumTableProps) {

  // https://github.com/TanStack/table/discussions/2336
  const navigate = useNavigate();
  const handleRowClick = (museum: any) => {
    navigate(`/museums/${museum.id}`);
  }

  if (props.data.length === 0) {
    return (
      <div style={{ textAlign: "center" }}>
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    )
  }

  var searchTerms: string[];
  if (props.highlight[0] === undefined) {
    searchTerms = []
  } else {
    searchTerms = props.highlight
  }

  return (
    <Table hover>
      <thead>
        <tr>
          <th>Name</th>
          <th>Country</th>
          <th>Visitors Per Year</th>
          <th>Address</th>
          <th>Founder</th>
          <th>Date Established</th>
          <th>Director</th>
          <th>Rating</th>
        </tr>
      </thead>
      <tbody>
        {props.data.map((item: any) => {
          return (
            <tr onClick={() => handleRowClick(item)}>
              <td>
                <Highlighter
                  searchWords={searchTerms}
                  autoEscape={true}
                  textToHighlight={item.name}
                >
                </Highlighter>
              </td>
              <td>
                <Highlighter
                  searchWords={searchTerms}
                  autoEscape={true}
                  textToHighlight={item.country}
                >
                </Highlighter>
              </td>
              <td>
                {comma_sep_num(item.visitors_per_year)}
              </td>
              <td>
                <Highlighter
                  searchWords={searchTerms}
                  autoEscape={true}
                  textToHighlight={item.address}
                >
                </Highlighter>
                {item.address}
              </td>
              <td>
                <Highlighter
                  searchWords={searchTerms}
                  autoEscape={true}
                  textToHighlight={item.founder}
                >
                </Highlighter>

              </td>
              <td>
                <Highlighter
                  searchWords={searchTerms}
                  autoEscape={true}
                  textToHighlight={item.date_established}
                >
                </Highlighter>

              </td>
              <td>
                <Highlighter
                  searchWords={searchTerms}
                  autoEscape={true}
                  textToHighlight={item.director}
                >
                </Highlighter>

              </td>
              <td>
                {item.rating}
              </td>
            </tr>
          );
        })}

      </tbody>
    </Table>
  )
}

function MuseumPage() {

  const [searchParams, setSearchParams] = useSearchParams();

  const [apiDataLoaded, setApiDataLoaded] = useState(false)
  const [apiData, setApiData] = useState<any[]>([])
  const [apiDataLength, setApiDataLength] = useState(0)

  const [highlightInput, setHighlightInput] = useState([]);

  const fetchData = () => {
    return fetch("https://api.artdb.me/museums?" + searchParams.toString())
      .then((response) => response.json())
      .then((responseJson) => {
        setApiData(responseJson.data)
        setApiDataLength(responseJson.total)
        setHighlightInput(responseJson.highlight)
      })
      .finally(() => setApiDataLoaded(true))
  }

  useEffect(() => {
    fetchData()
  }, [searchParams])

  const currentPage = useMemo(() => {
    return Number(searchParams.get("page") ?? 1)
  }, [searchParams])

  const pageSize = useMemo(() => {
    return Number(searchParams.get("per_page") ?? 20)
  }, [searchParams])

  const currentData = useMemo(() => {
    if (apiDataLoaded) {
      return apiData;
    } else {
      return []
    }
  }, [apiDataLoaded, apiData]);

  return (
    <Container>
      <Row className="title">
        Museums
      </Row>
      <Row className="space" />
      <SearchBar
        searchParams={searchParams}
        searchParamsChange={setSearchParams}
        sortableFields={["name", "date_established", "visitors_per_year", "rating"]}
        filterData={filterData}  />

      <MuseumTable data={currentData} highlight={highlightInput} />
      <PageControl
        currentPage={currentPage}
        totalCount={apiDataLength}
        pageSize={pageSize}
        dataLoaded={apiDataLoaded}
        searchParams={searchParams}
        searchParamsChange={setSearchParams}
      />
    </Container>
  )
}
export default MuseumPage;
