
import { Row, Card, Container } from 'react-bootstrap'
import { Link, useSearchParams } from 'react-router-dom';
import { useState, useMemo, useEffect } from 'react';
import { PageControl } from './../../components/ModelPagination'
import Spinner from 'react-bootstrap/Spinner';
import { SearchBar } from '../../components/SearchBar';
import Highlighter from 'react-highlight-words';

import './Grid.css'
import filterData from './artworkFilters.json';

export interface ArtworkProps {
  title: string;
  url: string;
  artist: string;
  genre: string;
  museum: string;
  location: number;
  link: string;
  date: string;
  country: string;
  type: string;
  medium: string;
  highlight: string[];
}

export function ArtworkCard(props: ArtworkProps) {
  
  var searchTerms: string[];
  if(props.highlight[0] === undefined) {
    searchTerms = []
  } else {
    searchTerms = props.highlight
  }

  return (
    <Link to={props.link} style={{ height: "350px" }}>
      <Card className="expand" border="dark" style={{ fontSize: '11px' }} >
        <Row className="justify-content-md-center">
          <img
            alt={props.title}
            src={props.url ? props.url : "/noimage.png"}
            height="200px"
            style={{ width: "auto" }} />
        </Row>
        <Card.Footer style={{ height: "150px" }}>
          <Card.Title className='card-title'>
            <Highlighter className='text-truncate'
                searchWords = {searchTerms}
                autoEscape={true}
                textToHighlight={props.title}
            >
            </Highlighter>
            {}
          </Card.Title>
          <Card.Subtitle>
            <Highlighter
                searchWords = {searchTerms}
                autoEscape={true}
                textToHighlight={props.artist + ", " + props.date}
            >
            </Highlighter>
          </Card.Subtitle>
          <Card.Text className='text-truncate'>
            <Highlighter
                searchWords = {searchTerms}
                autoEscape={true}
                textToHighlight={props.country}
            >
            </Highlighter>
          </Card.Text>
          <Card.Text className='text-truncate'>
            <Highlighter
                searchWords = {searchTerms}
                autoEscape={true}
                textToHighlight={props.genre}
            >
            </Highlighter>
          </Card.Text>
          <Card.Text className='text-truncate'>
            Medium: <Highlighter
                searchWords = {searchTerms}
                autoEscape={true}
                textToHighlight={props.medium}
            >
            </Highlighter>
          </Card.Text>
          <Card.Text className='text-truncate'>
            Owned by: <Highlighter
                searchWords = {searchTerms}
                autoEscape={true}
                textToHighlight={props.museum}
            >
            </Highlighter>
          </Card.Text>
        </Card.Footer>
      </Card>
    </Link>
  )
}

export interface ArtworkGridProps {
  data: any
  highlight: string[]
}

export function ArtworkGrid(props: ArtworkGridProps) {
  if (props.data.length === 0) {
    return (
      <div style={{textAlign: "center"}}>
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    )
  }
  return (

    <Container style={{ width: '100%' }} >
      {/* <CardGroup> */}
      <Row xs={1} md={2} lg={3} xl={4} className='g-3'  >
        {props.data.map((item: any) => {
          return (
            <ArtworkCard
              title={item.name }
              artist={item.artist_name}
              date={item.date_of_creation}
              country={item.country_of_origin}
              genre={item.category}
              museum={item.owning_museum}
              location={item.location}
              type={item.type}
              medium={item.medium}
              url={item.image_url}
              link={"/artworks/" + item.id}
              highlight={props.highlight}
            />
          );
        })}
      </Row>
    </Container>
  )
}

function ArtworkPage() {

  const [searchParams, setSearchParams] = useSearchParams();

  const [apiDataLoaded, setApiDataLoaded] = useState(false)
  const [apiData, setApiData] = useState<any[]>([])
  const [apiDataLength, setApiDataLength] = useState(0)

  const [highlightInput, setHighlightInput] = useState<string[]>([]);

  const fetchData = () => {
    return fetch("https://api.artdb.me/artworks?" + searchParams.toString())
      .then((response) => response.json())
      .then((responseJson) => {
        setApiData(responseJson.data)
        setApiDataLength(responseJson.total)
        setHighlightInput(responseJson.highlight)
      })
      .finally(() => setApiDataLoaded(true))
  }

  useEffect(() => {
    fetchData()
  }, [searchParams])

  const currentPage = useMemo(() => {
    return Number(searchParams.get("page") ?? 1)
  }, [searchParams])

  const pageSize = useMemo(() => {
    return Number(searchParams.get("per_page") ?? 20)
  }, [searchParams])

  const currentData = useMemo(() => {
    if (apiDataLoaded) {
      return apiData
    } else {
      return []
    }
  }, [apiDataLoaded, apiData]);

  return (
    <Container className='center'>
      <div className="title">
        Artworks
      </div>
      <Row className="space" />

      <SearchBar
        searchParams={searchParams}
        searchParamsChange={setSearchParams}
        sortableFields={["name", "date_of_creation"]}
        filterData={filterData} />
      <ArtworkGrid data={currentData} highlight={highlightInput}/>
      <Row className="space" />
      <PageControl
        currentPage={currentPage}
        totalCount={apiDataLength}
        pageSize={pageSize}
        dataLoaded={apiDataLoaded}
        searchParams={searchParams}
        searchParamsChange={setSearchParams}

      />
      <Row className="space" />
    </Container>
  )
}
export default ArtworkPage;
