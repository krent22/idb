
import { Row, Col, Card, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import './Grid.css'


function MuseumGrid() {
  return (
    <Container>
      <Row>
        <div className="card border-0">
          <Card className="title-card" border="light">
            <Card.Title>
              Museums
            </Card.Title>
            <Card.Text>
              Total Instances: 3
            </Card.Text>
          </Card>
        </div>
      </Row>
      <Row>
        <Col>
          <Link to ="/chicago-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Chicago Institute of Art"
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Art_Institute_of_Chicago_%2851575570710%29.jpg/1920px-Art_Institute_of_Chicago_%2851575570710%29.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Art Institute of Chicago
                </Card.Title>
                <Card.Subtitle>
                  United States of America
                </Card.Subtitle>
                <Card.Text>
                  Year Founded: 1879
                </Card.Text>
                <Card.Text>
                  Visitors/yr: 1.5 milliion
                </Card.Text>
                <Card.Text>
                  Donation Size: $86 million USD
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
        <Col>
          <Link to ="/cleveland-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Cleveland Museum of Art"
                    src="https://upload.wikimedia.org/wikipedia/commons/2/29/Springtime_art_museum.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Cleveland Institute of Art
                </Card.Title>
                <Card.Subtitle>
                  United States of America
                </Card.Subtitle>
                <Card.Text>
                  Year Founded: 1913
                </Card.Text>
                <Card.Text>
                  Visitors/yr: 409,921
                </Card.Text>
                <Card.Text>
                  Donation Size: $17 million USD
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
        <Col>
          <Link to ="/met-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Metropolitan Museum of Art"
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Metropolitan_Museum_of_Art_%28The_Met%29_-_Central_Park%2C_NYC.jpg/1920px-Metropolitan_Museum_of_Art_%28The_Met%29_-_Central_Park%2C_NYC.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Metropolitan Museum of Art
                </Card.Title>
                <Card.Subtitle>
                  United States of America
                </Card.Subtitle>
                <Card.Text>
                  Year Founded: 1870
                </Card.Text>
                <Card.Text>
                  Visitors/yr: 5 million
                </Card.Text>
                <Card.Text>
                  Donation Size: $234 million USD
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}
export default MuseumGrid;
