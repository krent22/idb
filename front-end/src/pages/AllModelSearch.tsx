
import { Row, Col, Card, Container, CardGroup } from 'react-bootstrap'
import { Link, useSearchParams } from 'react-router-dom';
import { useState, useMemo, useEffect } from 'react';

import { ArtworkGrid } from './models/ArtworkGrid';
import { ArtistTable } from './models/ArtistTable';
import { MuseumTable } from './models/MuseumTable';

import Highlighter from 'react-highlight-words';
// import Highlight from './search/Highlight';


import "./instances/InstanceCards.css"

function Search() {

  const [searchParams] = useSearchParams();

  const [apiArtworkDataLoaded, setApiArtworkDataLoaded] = useState(false)
  const [apiArtworkData, setApiArtworkData] = useState<any[]>([])
  const [apiArtworkDataLength, setApiArtworkDataLength] = useState(0)
  const [artworkHighlightedInput, setArtworkHighlightedInput] = useState([])

  const [apiArtistDataLoaded, setApiArtistDataLoaded] = useState(false)
  const [apiArtistData, setApiArtistData] = useState<any[]>([])
  const [apiArtistDataLength, setApiArtistDataLength] = useState(0)
  const [artistHighlightedInput, setArtistHighlightedInput] = useState([])


  const [apiMuseumDataLoaded, setApiMuseumDataLoaded] = useState(false)
  const [apiMuseumData, setApiMuseumData] = useState<any[]>([])
  const [apiMuseumDataLength, setApiMuseumDataLength] = useState(0)
  const [museumHighlightedInput, setMuseumHighlightedInput] = useState([])


  const fetchArtworkData = () => {
    return fetch("https://api.artdb.me/artworks?" + searchParams.toString())
      .then((response) => response.json())
      .then((responseJson) => {
        setApiArtworkData(responseJson.data)
        setApiArtworkDataLength(responseJson.total)
        setArtworkHighlightedInput(responseJson.highlight)
      })
      .finally(() => setApiArtworkDataLoaded(true))
  }

  const fetchArtistData = () => {
    return fetch("https://api.artdb.me/artists?" + searchParams.toString())
      .then((response) => response.json())
      .then((responseJson) => {
        setApiArtistData(responseJson.data)
        setApiArtistDataLength(responseJson.total)
        setArtistHighlightedInput(responseJson.highlight)
      })
      .finally(() => setApiArtistDataLoaded(true))
  }

  const fetchMuseumData = () => {
    return fetch("https://api.artdb.me/museums?" + searchParams.toString())
      .then((response) => response.json())
      .then((responseJson) => {
        setApiMuseumData(responseJson.data)
        setApiMuseumDataLength(responseJson.total)
        setMuseumHighlightedInput(responseJson.highlight)
      })
      .finally(() => setApiMuseumDataLoaded(true))
  }

  useEffect(() => {
    fetchArtworkData()
    fetchArtistData()
    fetchMuseumData()
  }, [])

  const currentArtworkData = useMemo(() => {
    if (apiArtworkDataLoaded) {
      return apiArtworkData.slice(0, 3)
    } else {
      return []
    }
  }, [apiArtworkDataLoaded, apiArtworkData]);

  const currentArtistData = useMemo(() => {
    if (apiArtistDataLoaded) {
      return apiArtistData.slice(0, 3)
    } else {
      return []
    }
  }, [apiArtistDataLoaded, apiArtistData]);

  const currentMuseumData = useMemo(() => {
    if (apiMuseumDataLoaded) {
      return apiMuseumData.slice(0, 3)
    } else {
      return []
    }
  }, [apiMuseumDataLoaded, apiMuseumData]);

  return (

    <Container className='center'>
      <Col>

        <Row className='title' style={{ height: "60px" }} >
          Artworks
        </Row>
        <ArtworkGrid data={currentArtworkData} highlight={artworkHighlightedInput}/>
        <Row style={{textAlign: "center", justifyContent: "center"}}>
          {apiArtworkDataLength} results. <Link to={`/artworks?${searchParams.toString()}`} className='customLink'>View All</Link>
        </Row>

        <Row className='title' style={{ height: "60px" }}>
          Artists
        </Row>
        <ArtistTable data={currentArtistData} highlight={artistHighlightedInput}/>
        <Row style={{textAlign: "center", justifyContent: "center"}}>
          {apiArtistDataLength} results. <Link to={`/artists?${searchParams.toString()}`} className='customLink'>View All</Link>
        </Row>

        <Row className='title'  style={{ height: "60px" }}>
          Museums
        </Row>
        <MuseumTable data={currentMuseumData} highlight={museumHighlightedInput}/>
        <Row style={{textAlign: "center", justifyContent: "center"}}>
          {apiMuseumDataLength} results. <Link to={`/museums?${searchParams.toString()}`} className='customLink'>View All</Link>
        </Row>
      </Col>

    </Container>
  )
}
export default Search;
