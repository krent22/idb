
import { Row, Col, Card, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import './Grid.css'


function ArtistGrid() {
  return (
    <Container>
      <Row>
        <div className="card border-0">
          <Card className="title-card" border="light">
            <Card.Title>
              Artists
            </Card.Title>
            <Card.Text>
              Total Instances: 3
            </Card.Text>
          </Card>
        </div>
      </Row>
      <Row>
        <Col>
          <Link to ="/alma-thomas-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Starry Night and the Astronauts"
                    src="https://www.artic.edu/iiif/2/e966799b-97ee-1cc6-bd2f-a94b4b8bb8f9/full/600,/0/default.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Alma Thomas
                </Card.Title>
                <Card.Subtitle>
                  American
                </Card.Subtitle>
                <Card.Text>
                  Female
                </Card.Text>
                <Card.Text>
                  1891
                </Card.Text>
                <Card.Text>
                  1978
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
        <Col>
          <Link to ="/du-jin-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Du Jin"
                    src="https://openaccess-cdn.clevelandart.org/1954.582/1954.582_web.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Du Jin
                </Card.Title>
                <Card.Subtitle>
                  Chinese
                </Card.Subtitle>
                <Card.Text>
                  Male
                </Card.Text>
                <Card.Text>
                  1446
                </Card.Text>
                <Card.Text>
                  1524
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
        <Col>
          <Link to ="/sadeler-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Johann Sadeler I"
                    src="https://images.metmuseum.org/CRDImages/dp/original/DP869284.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Johann Sadeler I
                </Card.Title>
                <Card.Subtitle>
                  Flemish
                </Card.Subtitle>
                <Card.Text>
                  Male
                </Card.Text>
                <Card.Text>
                  1550
                </Card.Text>
                <Card.Text>
                  1601
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}
export default ArtistGrid;
