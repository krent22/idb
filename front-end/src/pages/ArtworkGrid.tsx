
import { Row, Col, Card, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import './Grid.css'


function ArtworkGrid() {
  return (
    <Container>
      <Row>
        <div className="card border-0">
          <Card className="title-card" border="light">
            <Card.Title>
              Artworks
            </Card.Title>
            <Card.Text>
              Total Instances: 3
            </Card.Text>
          </Card>
        </div>
      </Row>
      
      <Row>
        <Col>
          <Link to ="/starry-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Starry Night and the Astronauts"
                    src="https://www.artic.edu/iiif/2/e966799b-97ee-1cc6-bd2f-a94b4b8bb8f9/full/600,/0/default.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Starry Night and the Astronauts
                </Card.Title>
                <Card.Subtitle>
                  Alma Thomas
                </Card.Subtitle>
                <Card.Text>
                  Contemporary Art
                </Card.Text>
                <Card.Text>
                  United States
                </Card.Text>
                <Card.Text>
                  1972
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
        <Col>
        <Link to ="/wandering-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="The Poet Lin Bu Wandering in the Moonlight"
                    src="https://openaccess-cdn.clevelandart.org/1954.582/1954.582_web.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  The Poet Lin Bu Wandering in the Moonlight
                </Card.Title>
                <Card.Subtitle>
                  Du Jin
                </Card.Subtitle>
                <Card.Text>
                  China, Ming dynasty (1368–1644)
                </Card.Text>
                <Card.Text>
                  China
                </Card.Text>
                <Card.Text>
                  1460
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
        <Col>
        <Link to ="/neptune-instance" >
            <Card className="artwork-card" border="dark">
              <Card.Body>
                <Row className="justify-content-md-center">
                  <Card.Img 
                    alt="Neptune and Caenis"
                    src="https://images.metmuseum.org/CRDImages/es/original/ES7369.jpg"
                    height="200px"
                    style={{width: "auto"}} />
                  </Row>
              </Card.Body>
              <Card.Footer>
                <Card.Title>
                  Neptune and Caenis
                </Card.Title>
                <Card.Subtitle>
                  Johann Sadeler I
                </Card.Subtitle>
                <Card.Text>
                  European Sculpture and Decorative Arts
                </Card.Text>
                <Card.Text>
                  Europe
                </Card.Text>
                <Card.Text>
                  1585
                </Card.Text>
              </Card.Footer>
            </Card>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}
export default ArtworkGrid;
