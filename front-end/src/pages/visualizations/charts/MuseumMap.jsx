import { scaleLinear } from "d3-scale";
import { csv } from "d3-fetch";
import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";

import {
  ComposableMap,
  Geographies,
  Geography,
  Sphere,
  Graticule,
  ZoomableGroup
} from "react-simple-maps";


const geoUrl = "/features.json";

const colorScale = scaleLinear()
  .domain([1, 84])
  .range(["#fcd9c7", "#67000d"]);

function MuseumMap(props) {
  console.log("props")
  console.log(props)

  return (
    <ComposableMap
      projectionConfig={{
        rotate: [-10, 0, 0],
        scale: 147
      }}
    >
      <ZoomableGroup disablePanning zoom={1}>
      <Sphere stroke="#E4E5E6" strokeWidth={0.5} id='0' fill={"#9bbff4"}/>
      {props.data.length > 0 && (
        <Geographies geography={geoUrl}>
          {({ geographies }) =>
            geographies.map((geo) => {
              let d = Object.assign({}, props.data.find((s) => s.ISO3 === geo.id));
              
              if (d.country === undefined) {
                d = Object.assign({}, {ISO3: "", frequency: 0, Country: ""})
              }
              
              return (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  style={{
                    default: {
                      fill: d.frequency > 0.0 ? colorScale(d.frequency) : "white",
                    }, 
                  }}
                />
              );
            })
          }
        </Geographies>
      )}
      </ZoomableGroup>
    </ComposableMap>
  );
};


export default MuseumMap;
