import PropTypes from 'prop-types';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


interface ArtworkProps {
    data: any,
}

function ArtworkLineChart(props: ArtworkProps) {
    const data = props.data;
    return (
        <div style={{height: 500}}>
        <ResponsiveContainer>
            <LineChart width={500} height={250} data={data}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="Year" />
            <YAxis dataKey="Artworks"/>
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="Artworks" stroke="#8884d8" />
            </LineChart>
        </ResponsiveContainer>
        </div>
    );
}
export default ArtworkLineChart;
