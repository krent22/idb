import React, {useEffect, useState} from "react";
import { csv } from "d3-fetch";
import {
  ComposableMap,
  Geographies,
  Geography,
  Marker,
  ZoomableGroup
} from "react-simple-maps";

const geoUrl = "/TexasCounties.json";
function TexasCityStoreFreqMap(props) {
  console.log(props.data)

  return (
    <ComposableMap
      projection="geoMercator"
      projectionConfig={{
        center: [-100, 32],
        scale: 1500
      }}
      width={800}
      height={400}
      style={{ width: "100%", height: "auto" }} 
    >
      <ZoomableGroup disablePanning zoom={1}>
      <Geographies geography={geoUrl}>
        {({ geographies }) =>
          geographies.map((geo) => (
            <Geography
              key={geo.rsmKey}
              geography={geo}
              fill="#DDD"
              stroke="#FFF"
            />))
        }
      </Geographies>
      {props.data.map(({ City, Coordinate, Frequency }) => (
        <Marker key={City} coordinates={Coordinate}
        style={{ 
          default: {
            fontFamily: "system-ui", 
            fill: "#5D5A6D",
            outline: "none"
          },
          hover: {
            fontFamily: "system-ui",
            fill: "#fd5a6d",
            outline: "1px solid black"
          } 
        }}>
          <circle r={Frequency} fill="#F00" stroke="#fff" strokeWidth={1} />
          <text
            textAnchor="middle"
            y={0}
          >
          </text>
        </Marker>
      ))}
      </ZoomableGroup>
    </ComposableMap>
  );
};

export default TexasCityStoreFreqMap;
