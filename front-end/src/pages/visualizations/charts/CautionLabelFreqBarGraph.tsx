import PropTypes from 'prop-types';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


interface CautionProps {
    data: any[],
}

function CautionLabelFreqBarGraph(props: CautionProps) {
    const data = props.data;

    return (
        <div style={{height: 500}}>

            <ResponsiveContainer>
            <BarChart
                width={500}
                height={500}
                data={data}
                margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="caution" />
                <YAxis dataKey = "Frequency" />
                <Tooltip />
                <Legend />
                <Bar dataKey="Frequency" fill="#8884d8" />
            </BarChart>
            </ResponsiveContainer>
        </div>
      );
}

export default CautionLabelFreqBarGraph;
  
CautionLabelFreqBarGraph.propTypes = {
    data: PropTypes.array,
};
