import PropTypes from 'prop-types';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


interface ArtistProps {
    data: any,
}

function ArtistBarChart(props: ArtistProps) {
    const data = props.data;

    return (
        <div style={{height: 500}}>

            <ResponsiveContainer>
            <BarChart
                width={500}
                height={500}
                data={data}
                margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="Gender" />
                <YAxis dataKey="Artworks" />
                <Tooltip />
                <Legend />
                <Bar dataKey="Artworks" fill="#8884d8" />
            </BarChart>
            </ResponsiveContainer>
        </div>
      );
}

export default ArtistBarChart;
