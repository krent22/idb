import { useState, useEffect } from "react";
import Visualization from './Visualization';

function VisualizationData() {

    const formatArtistData = (arr: any) => {
      const d: any[] = [];
      let keys = arr['gender'];
      let values = arr['#artworks'];
      const len = values.length
      
      for (var idx = 0; idx < len; idx++) {
        d.push({ 
          Gender: keys[idx],
          Artworks: values[idx],
      });
      }
      return d;
    }

    const formatArtworkData = (arr: any) => {
      const d: any[] = [];
      let keys = arr['year'];
      let values = arr['#artworks'];
      const len = values.length
      
      for (var idx = 0; idx < len; idx++) {
        d.push({ 
          Year: keys[idx],
          Artworks: values[idx],
      });
      }
      return d;
    }

    const ISO3Mappings = {
      "United States of America" : "USA",
      "People's Republic of China" : "CHN",
      "Argentina" : "ARG",
      "Spain" : "ESP",
      "Switzerland" : "CHE",
      "Hungary" : "HUN",
      "Belgium" : "BEL",
      "Sweden" : "SWE",
      "France" : "FRA",
      "United Kingdom" : "GBR",
      "Israel" : "ISR",
      "Netherlands" : "NLD",
      "Austria" : "AUT",
      "Australia" : "AUS",
      "Germany" : "DEU",
      "Japan" : "JPN",
      "Denmark" : "DNK",
      "Canada" : "CAN",
      "South Korea" : "KOR"
    };
    const convISO3: Map<string, string> = new Map(Object.entries(ISO3Mappings))


    const formatMuseumData = (arr: any) => {
      const d: any[] = [];
      let keys = arr['country'];
      let values = arr['#museums'];
      const len = values.length
      
      for (var idx = 0; idx < len; idx++) {
        d.push({ 
          ISO3: convISO3.get(keys[idx]),
          frequency: values[idx],
          country: keys[idx],
        });
      }
      return d;
    }

      
    const [artistData, setArtistData] = useState<any[]>([])
    const [artistDataLoaded, setArtistDataLoaded] = useState(false)
  
    const [artworkData, setArtworkData] = useState<any[]>([])
    const [artworkDataLoaded, setArtworkDataLoaded] = useState(false)

    const [museumData, setMuseumData] = useState<any[]>([])
    const [museumDataLoaded, setMuseumDataLoaded] = useState(false)
    
    // Iterate through all three: museums, artworks, artists vis_data and set them to their respective data lists.
    const fetchArtistVisData = () => {
      return fetch(`https://api.artdb.me/artists?vis_data`)
        .then((response) => response.json())
        .then((responseJson) => {
          setArtistData(formatArtistData(responseJson.vis_data))
        })
        .finally(() => setArtistDataLoaded)
    }
    
    const fetchArtworkVisData = () => {
      return fetch(`https://api.artdb.me/artworks?vis_data`)
        .then((response) => response.json())
        .then((responseJson) => {
          setArtworkData(formatArtworkData(responseJson.vis_data))
        })
        .finally(() => setArtworkDataLoaded)
    }
  
    const fetchMuseumVisData = () => {
      return fetch(`https://api.artdb.me/museums?vis_data`)
        .then((response) => response.json())
        .then((responseJson) => {
          setMuseumData(formatMuseumData(responseJson.vis_data))
        })
        .finally(() => setMuseumDataLoaded)
    }
  
    useEffect(() => {
      fetchArtistVisData()
      fetchArtworkVisData()
      fetchMuseumVisData()
    }, [])

    return (
      <Visualization artistData={artistData} artworkData={artworkData} museumData={museumData}/>
    )
  }
export default VisualizationData;
