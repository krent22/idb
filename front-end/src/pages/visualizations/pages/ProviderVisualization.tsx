import { Card, Table, Container } from 'react-bootstrap';
import CautionLabelFreqBarGraph from '../charts/CautionLabelFreqBarGraph';
import CookTimeHistogram from '../charts/CookTimeHistogram';
import TexasCityStoreFreqMap from '../charts/TexasCityStoreFreqMap';


interface ProVizProps {
  cautionData: any[],
  cookTimeData: any[],
  storeFreqData: any[]
}

function ProviderVisualization(props: ProVizProps) {
    return (
      <Container >    
        <Card className="chart-card" style = {{marginTop: "30px"}}>
          <Card.Title style = {{ marginTop: "15px"}}>
            Ingredient Caution Label Frequency 
          </Card.Title>
          <Card.Body>
            <CautionLabelFreqBarGraph data={props.cautionData} />
          </Card.Body>
        </Card>

        <Card className="chart-card" style = {{marginTop: "30px"}}>
          <Card.Title style = {{ marginTop: "15px"}}>
            Recipe Cook Time Frequency 
          </Card.Title>
          <Card.Body>
            <CookTimeHistogram data={props.cookTimeData} />
          </Card.Body>
        </Card>

        <Card className="chart-card text-center" style = {{marginTop: "30px", marginBottom: "30px"}}>
          <Card.Title style = {{ marginTop: "15px"}}>
            Texas City Store Frequency
          </Card.Title>
          <Card.Text>
            Explanation: The bigger the dot, the more stores are located in that city.
          </Card.Text>
          <Card.Body>
            <TexasCityStoreFreqMap data={props.storeFreqData} />
          </Card.Body>
        </Card>

      </Container>
    );
}

export default ProviderVisualization;
