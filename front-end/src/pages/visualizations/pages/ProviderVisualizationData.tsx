import { useState, useEffect } from "react";
import ProviderVisualization from './ProviderVisualization';


function ProviderVisualizationData() {

    const [cautionData, setCautionData] = useState<any[]>([])
    const [cookData , setCookData] = useState<any[]>([])
    const [storeFreqData, setStoreFreqData] = useState<any[]>([])

    const formatCautionData = (arr: any) => {
        const d: any[] = [];
        let keys = arr.caution;
        let values = arr.frequency;
        const len = values.length
        
        for (var idx = 0; idx < len; idx++) {
          d.push({ 
            caution: keys[idx],
            Frequency: values[idx],
        });
        }
        return d;
      }
  
      const formatCookData = (arr: any) => {
        const d: any[] = [];
        const counts: any[] = [];
        let values = arr['cook_times'];
        const len = values.length
        
        for (var idx = 0; idx < len; idx++) {
          // https://www.tutorialspoint.com/counting-unique-elements-in-an-array-in-javascript
          counts[values[idx]] = (counts[values[idx]] || 0) + 1
        }

        for(var idx = 0; idx < counts.length; idx++) {
          if(counts[idx] !== undefined) {
            d.push({ 
              Time: idx,
              Frequency: counts[idx],
            });
          }
        }
        return d;
      }
  
      const formatStoreFreqData = (arr: any) => {
        const d: any[] = [];
        const len = arr.length
        
        for (var idx = 0; idx < len; idx++) {
          let place = d.find((s) => s.City === arr[idx].city);
          if(place === undefined) {
            d.push({
              City: arr[idx].city, 
              Coordinate: new Array(arr[idx].longitude, arr[idx].latitude), 
              Frequency: 1
            })
          } else {
            place.Frequency += 1
          }
        }
        return d;
      }

    const fetchProviderVisData = () => {
    return fetch(`https://api.artdb.me/developer_viz`)
        .then((response) => response.json())
        .then((responseJson) => {
            setCautionData(formatCautionData(responseJson.ingredient_viz))
            setCookData(formatCookData(responseJson.recipe_viz))
        })
    }

    const fetchProviderData = () => {
      return fetch(`https://api.mealmaker.me/stores?page=1&perPage=200`)
        .then((response) => response.json())
        .then((responseJson) => {
          setStoreFreqData(formatStoreFreqData(responseJson.page))
        })
    }

    useEffect(() => {
        fetchProviderVisData()
        fetchProviderData()
    }, [])

    return (
      <ProviderVisualization cautionData={cautionData} cookTimeData={cookData} storeFreqData={storeFreqData}/>
    )
}

export default ProviderVisualizationData;