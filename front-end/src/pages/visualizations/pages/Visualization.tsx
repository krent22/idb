import { Card, Row, Container } from 'react-bootstrap';
import ArtworkLineChart from '../charts/ArtworkLineChart';
import ArtistBarChart from '../charts/ArtistBarChart';
import MuseumMap from '../charts/MuseumMap';



interface VizProps {
  artistData: any[],
  artworkData: any[],
  museumData: any[]
}

function Visualization(props: VizProps) {
  return (
    <Container>    
      <Card className="chart-card" style = {{marginTop: "30px"}}>
        <Card.Title style = {{ marginTop: "15px"}}>
        Artist Gender Frequency by Artwork 
        </Card.Title>
        <Card.Body>
          <ArtistBarChart data={props.artistData} />
        </Card.Body>
      </Card>

      <Card className="chart-card" style = {{marginTop: "30px"}}>
        <Card.Title style = {{ marginTop: "15px"}}>
        Artwork Frequency by Year 
        </Card.Title>
        <Card.Body>
          <ArtworkLineChart data={props.artworkData} />
        </Card.Body>
      </Card>
      
      <Card className="chart-card text-center" style = {{marginTop: "30px", marginBottom: "30px"}}>
        <Card.Title style = {{ marginTop: "15px"}}>
        Museum Frequency Heatmap
        </Card.Title>
        <Card.Text>
          Explanation: The darker the color on the country, the more museums are located in that country.
        </Card.Text>
        <Card.Body>
          <MuseumMap data={props.museumData}/>
        </Card.Body>
      </Card>
      
    </Container>
  );
}

export default Visualization;
