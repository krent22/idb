// Based off of code from GetThatBread
// Map and videos took inspiration from UniverCity
// Map and Video code provided by YouTube and GoogleMaps
// Table code from React Bootstrap documentation
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import './InstanceCards.css'
import MuseumLayout from './layouts/MuseumLayout'

function MuseumInstance() {

  const { id } = useParams();

  const [apiDataLoaded, setApiDataLoaded] = useState(false)
  const [apiData, setApiData] = useState<any[]>([])

  const fetchData = () => {
    return fetch(`https://api.artdb.me/museums/${id}`)
      .then((response) => response.json())
      .then((responseJson) => {
        setApiData(responseJson.data)
      })
      .finally(() => setApiDataLoaded(true))
  }

  useEffect(() => {
    fetchData()
  }, [id])

  if (!apiDataLoaded) {
    return (
      <div />
    )
  } else {
    return (
      <MuseumLayout data={apiData} />
    )
  }
}
export default MuseumInstance;