import { convertToBCE } from '../InstanceConversions'
import { Row, Col, Card, Container, Table, Carousel } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import '../InstanceCards.css'

interface ArtistInstanceProps {
    data: any
}
  
export function ArtistLayout(props: ArtistInstanceProps) {
    
    return (
      <Container>
        <Row className="mt-4" style={{ marginBottom: "30px" }}>
          <Col md={{ span: 8, offset: 2 }}>
            <Row>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Row className="mb-4">
                    <div className='title'>
                      {props.data.name}
                    </div>
                  </Row>
                  <Row className="space" />

                  <Col className="mb-4">
                    <img
                        alt={props.data.name}
                        src={props.data.image}
                        height="500px"
                        style={{ width: "auto" }}
                      />
                  </Col>
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Artist Facts</Card.Title>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Artist Name: </span>
                      <span>{props.data.name}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Other Names Used: </span>
                      <span>{props.data.other_names}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Status: </span>
                      <span>{props.data.status}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Birth Country: </span>
                      <span>{props.data.birth_place}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Date of Birth: </span>
                      <span>{convertToBCE(props.data.birth_date)}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Date of Death: </span>
                      <span>{convertToBCE(props.data.death_date)}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Gender: </span>
                      <span>{props.data.gender}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Ethnicity: </span>
                      <span>{props.data.ethnicity}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Biography: </span>
                      <span>{props.data.biography}</span>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Career</Card.Title>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Number of Works: </span>
                      <span>{props.data.number_of_works}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Works: </span>
                    </Card.Text>
                    <Card.Body style={{ marginTop: "0px", marginBottom: "-30px" }}>
                      <Carousel>
                        {props.data.works.map((item: any) => {
                          return (
                            <Carousel.Item>
                              <img
                                className="carousel-image"
                                alt={item.artwork_name}
                                src={item.artwork_image}
                              />
                            </Carousel.Item>
                          );
                        })}
                      </Carousel>
                      <Table striped className="hover-table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Location</th>
                          </tr>
                        </thead>
                        <tbody>
                          {props.data.works.map((item: any) => {
                            return (
                              <tr>
                                <td>
                                  <Link to={"/artworks/" + item.artwork_id} className="customLink">{item.artwork_name}</Link>
                                </td>
                                <td>
                                  <Link to={"/museums/" + item.owning_museum_id} className="customLink">{item.owning_museum}</Link>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </Table>
                    </Card.Body>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
              {/* Map Media Type */}
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Artist Landmark</Card.Title>
                      <Card.Body>
                        <iframe
                          title="map"
                          height="450"
                          style={{ width: "100%", border: 0 }}
                          loading="lazy"
                          allowFullScreen
                          referrerPolicy="no-referrer-when-downgrade"
                          src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyCBufB9i-NiKC-pL9hP3-uGXLayYNro9_E&q=" + props.data.name}
                        />
                      </Card.Body>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }

export default ArtistLayout;