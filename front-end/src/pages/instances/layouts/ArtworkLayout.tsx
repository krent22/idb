import { Row, Col, Card, Container, Table } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import '../InstanceCards.css'

interface ArtworkInstanceProps {
    data: any
    relatedData:any
}

const limitTitle = (s : string) => {
  if (s.length < 20) {
    return s
  }

  return s.substring(0, 16) + "..."
}
  
export function ArtworkLayout(props: ArtworkInstanceProps) {
    return (
      <Container>
        <Row className="mt-4" style={{ marginBottom: "30px" }}>
          <Col md={{ span: 8, offset: 2 }}>
            <Row>
              <Col className="mb-4">
                <Card border="dark">
                  <Card.Body>
                    <Row className="justify-content-md-center">
                      <Card.Title style={{ marginBottom: "10px" }}>
                        {props.data.name}
                      </Card.Title>
                      <img
                        alt={props.data.name}
                        src={props.data.image_url}
                        height="500px"
                        style={{ width: "auto" }}
                      />
                    </Row>
                    <Row className="mt-4" >
                      <Card className="text-center" border="dark" style={{ marginBottom: "-11px" }}>
                        <Card.Body>
                          <Card.Subtitle style={{ marginBottom: "10px" }}>
                          <Link to={"/artists/" + props.data.artist_id} className="customLink">{props.data.artist_name}</Link>
                          </Card.Subtitle>
                          <Card.Text>
                            {props.data.description}
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </Row>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Artwork Attributes</Card.Title>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Artwork Name: </span>
                      <span>{props.data.name}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Artist Name: </span>
                      <span ><Link to={"/artists/" + props.data.artist_id} className="customLink">{props.data.artist_name}</Link></span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Artist Gender: </span>
                      <span>{props.data.artist_gender}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Artwork Place of Origin: </span>
                      <span>{props.data.country_of_origin}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Year of Creation: </span>
                      <span>{props.data.date_of_creation}</span>
  
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Category: </span>
                      <span>{props.data.category}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Dimensions: </span>
                      <span>{props.data.dimensions}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Type: </span>
                      <span>{props.data.type}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Medium: </span>
                      <span>{props.data.medium}</span>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Current Status</Card.Title>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Museum: </span>
                      <span ><Link to={"/museums/" + props.data.owning_museum_id} className="customLink">{props.data.owning_museum}</Link></span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Acquisition: </span>
                      <span>{props.data.acquisition}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Location: </span>
                      <span>{props.data.location}</span>
                    </Card.Text>
                    <Card.Body>
                      <iframe
                        title='map'
                        // width="600"
                        height="450"
                        style={{ width: "100%", border: 0 }}
                        loading="lazy"
                        allowFullScreen
                        referrerPolicy="no-referrer-when-downgrade"
                        src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyCBufB9i-NiKC-pL9hP3-uGXLayYNro9_E&q=" + props.data.owning_museum}
                      />
                    </Card.Body>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Exhibition History</Card.Title>
                    <Card.Body style={{ marginTop: "-30px", marginBottom: "-20px" }}>
                      <Table striped>
                        <thead>
                          <tr>
                            <th style={{ width: "50%" }}>Museum</th>
                            <th>Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          {props.data.exhibition_history.map((item: any) => {
                            return (
                              <tr>
                                <td>
                                  {/* Right now the museum_id doesn't exist*/ }
                                  <Link to={"/museums/" + item.museum_id} className="customLink">{item.museum_name}</Link>
                                </td>
                                <td>
                                  {item.exhibition_date}
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </Table>
                    </Card.Body>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Related Artworks</Card.Title>
                    <Card.Body style={{ marginBottom: "-30px" }}>
                      <Row>
                        {props.relatedData.filter((item:any, idx:number) => item.id != props.data.id && idx < 4)
                          .map((item: any) => {
                            return (
                              <Col className="mb-4">
                                <Link to={`/artworks/${item.id}`}>
                                  <Card className="expand" border="dark" style={{ fontSize: '11px', height: "185px", width: "240px"}} >
                                    <Row className="justify-content-center">
                                      <img
                                        alt={item.name}
                                        src={item.image_url ? item.image_url : "/noimage.png"}
                                        height="100px"
                                        style={{ width: "auto" }} />
                                    </Row>
                                    <Card.Footer style={{ height: "85px" }}>
                                      <Card.Title className='card-title'>
                                        {limitTitle(item.name)}
                                      </Card.Title>
                                      <Card.Subtitle style={{ marginTop: "2px" }}>
                                        {item.artist_name + ", " + item.date_of_creation}
                                      </Card.Subtitle>
                                     </Card.Footer>
                                  </Card>
                                </Link>
                              </Col>
                            );
                          })}
                      </Row>
                    </Card.Body>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }

export default ArtworkLayout;