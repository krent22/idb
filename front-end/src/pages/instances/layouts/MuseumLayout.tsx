import { Row, Col, Card, Container, Table } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import '../InstanceCards.css'
import { convertToBCE, comma_sep_num } from '../InstanceConversions'

interface MuseumInstanceProps {
    data: any
}
  
export function MuseumLayout(props: MuseumInstanceProps) {
  
    return (
      <Container>
        <Row className="mt-4" style={{ marginBottom: "30px" }}>
          <Col md={{ span: 8, offset: 2 }}>
            <Row>
              <Col className="mb-4">
                <Card border="dark">
                  <Card.Body>
                    <Row className="justify-content-md-center">
                      <Card.Title style={{ marginBottom: "10px" }}>
                          {props.data.name}
                      </Card.Title>
                      <img
                        alt={props.data.name}
                        src={props.data.image_exterior}
                        height="500px"
                        style={{ width: "auto" }}
                      />
                    </Row>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Museum Facts</Card.Title>
                    {/* <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Other names: </span>
                      <span> {props.data.name}</span>
                    </Card.Text> */}
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Region: </span>
                      <span>{props.data.country}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Date of Establishment: </span>
                      <span> {convertToBCE(props.data.date_established)}</span>
                    </Card.Text>
                    {/* Since collection_size is type varchar / object, harder to convert, probably try to cast it somehow... */}
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Collection Size: </span>
                      {/* TODO Figure out how to comma_sep_num this. */}
                      <span>{comma_sep_num(props.data.collection_size)}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Visitors Per Year: </span>
                      <span>{comma_sep_num(props.data.visitors_per_year)}</span>
                      
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Rating: </span>
                      <span>{props.data.rating}</span>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Contact Information</Card.Title>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Phone Number: </span>
                      <span>{props.data.phone_number}</span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Website: </span>
                      <span><a href={props.data.website}>{props.data.website}</a></span>
                    </Card.Text>
                    <Card.Text>
                      <span style={{ fontWeight: "bold" }}>Address: </span>
                      <span> {props.data.address}</span>
                    </Card.Text>
                    <Card.Body>
                      <iframe
                        title="map"
                        height="450"
                        style={{ width: "100%", border: 0 }}
                        loading="lazy"
                        allowFullScreen
                        referrerPolicy="no-referrer-when-downgrade"
                        src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyCBufB9i-NiKC-pL9hP3-uGXLayYNro9_E&q=" + props.data.name}
                      />
                    </Card.Body>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col className="mb-4">
                <Card className="text-center" border="dark">
                  <Card.Body>
                    <Card.Title className="attribute-card-title">Exhibited Works</Card.Title>
                    <Card.Body style={{ marginTop: "-30px", marginBottom: "-20px" }}>
                      <Table striped className="hover-table">
                        <thead>
                          <tr>
                            <th style={{ width: "50%" }}>Name</th>
                            <th>Artist</th>
                          </tr>
                        </thead>
                        <tbody>
                          {props.data.exhibition_history.map((item: any) => {
                            return (
                              <tr>
                                <td>
                                  <Link to={"/artworks/" + item.artwork_id} className="customLink">{item.artwork_name}</Link>
                                </td>
                                <td>
                                  <Link to={"/artists/" + item.artist_id} className="customLink">{item.artist_name}</Link>
                                </td>   
                              </tr>
                            );
                          })}
                        </tbody>
                      </Table>
                    </Card.Body>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }

export default MuseumLayout;