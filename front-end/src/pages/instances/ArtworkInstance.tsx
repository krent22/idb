// Based off of code from GetThatBread
// Map and videos took inspiration from UniverCity
// Map and Video code provided by YouTube and GoogleMaps
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import './InstanceCards.css'
import ArtworkLayout from './layouts/ArtworkLayout'

function ArtworkInstance() {

  const { id } = useParams();

  const [apiDataLoaded, setApiDataLoaded] = useState(false)
  const [apiData, setApiData] = useState<any>([])
  const [relatedData, setRelatedData] = useState<any[]>([])

  const fetchData = () => {
    return fetch(`https://api.artdb.me/artworks/${id}`)
      .then((response) => response.json())
      .then((responseJson) => {
        setApiData(responseJson.data)
        return responseJson.data
      }).then((data:any) => fetch(`https://api.artdb.me/artworks?per_page=4&query=
        ${data.category} ${data.type} ${data.country_of_origin}`)
              .then((relatedResponse) => relatedResponse.json())
              .then((relatedResponseJson) => {
                setRelatedData(relatedResponseJson.data.filter((item:any) => item.id !== data.id))
              }))
      .finally(() => setApiDataLoaded(true))
  }

  useEffect(() => {
    fetchData()
  },[id])

  if (!apiDataLoaded) {
    return (
      <div />
    )
  } else {
    return (
      <ArtworkLayout data={apiData} relatedData={relatedData}  />
    )
  }
}
export default ArtworkInstance;
