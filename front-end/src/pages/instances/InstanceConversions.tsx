// Converts negative dates to BC
export function convertToBCE(year: number | object) {
  
  if (typeof year === 'number') {
    var str: string = year.toString()
    let isNegative: boolean = str.charAt(0) == '-'
    var with_bc: string = ''
    if (isNegative) {
      with_bc = str.substring(1, str.length) + ' BCE'
      return with_bc
    } else {
      return year
    }
  } else if (typeof year === 'object') {
    return ''
  } else {
    return year
  }
}

// Format into tests.
// let test_cnst = 10
// let test_val_lim = 10000000
// for (var test_val = 100; test_val < test_val_lim; test_val = test_val * test_cnst) {
//     console.log(comma_sep_num(test_val))
// }

export function comma_sep_num(val: number | object) {
  if (val !== null && typeof val === 'number') {

    var str: string[] = Array.from(val.toString())
    str = str.reverse()
    var counter = 0
    while (counter < str.length - 1 && (str.length - counter - 1) > 2) {
        counter = counter + 3
        str.splice(counter, 0 , ',')
        counter = counter + 1
    }
    str = str.reverse()
  //   console.log(str)
    return str
  } else if (typeof val === 'object') {
    return ''
  } else {
    return val
  }

}
