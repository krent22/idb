import "./Splash.css";
import { Container, Card, Row, Col } from "react-bootstrap";

const models = {
  entry: [
    {
      name: "Artists",
      img: "vangogh.jpg",
      link: "/artists",
      description: "Search from a variety of artists!"
    },
    {
      name: "Artworks",
      img: "wheat_field_cypresses.jpg",
      link: "/artworks",
      description: "Enjoy an incredible depth and breadth of artworks!"
    },
    {
      name: "Museums",
      img: "Blanton Museum of Art.jpg",
      link: "/museums",
      description: "Learn more about museums across the globe!"
    }
  ]
};

const Homepage = () => {
  return (
    <div className="main-div">
      <div
        className="home-image"
        style={{ backgroundImage: "url(/contemporary.jpg)" }}
      >
        {" "}
      </div>

      <div className="text-place">
          <div className="multicolortext">
            <p className="center-justified">Welcome to Art DB!</p>
          </div>
      </div>

      <div className="bottom-div">
        <Container>
          <Row md={3}>
            {models.entry.map((item) => (
              <Col>
                <Card className="text-center splash-card" mt-3>
                  <Card.Img className="card-img" variant="top" src={item.img} />
                  <Card.Body>
                    <Card.Title>{item.name}</Card.Title>
                    {item.description}
                  </Card.Body>
                  <a href={item.link} className="stretched-link"></a>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Homepage;
