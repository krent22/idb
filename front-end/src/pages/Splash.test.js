import React from "react";
import { render, screen } from "@testing-library/react";
import { describe, expect, test } from '@jest/globals';
import Splash from "./Splash";

const card_desc = ["Welcome to Art DB!", "Artists", "Search from a variety of artists!", "Artworks", "Enjoy an incredible depth and breadth of artworks!", "Museums", "Learn more about museums across the globe!"]

for (let i = 0; i < card_desc.length; i++) {
  describe('Splash', () => {
    test("Splash text rendering", () => {
      render(<Splash />);
      expect(screen.getByText(card_desc[i])).toBeInTheDocument();
    });
  });
} 

