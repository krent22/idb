import axios from 'axios';
import { useEffect, useState } from 'react';
import Card from "react-bootstrap/Card";
import './About.css'

// Code based off of AnimalWatch repository: https://gitlab.com/JohnPowow/animalwatch/-/blob/main/frontend/src/pages/About.js

var totalIssues = 0;
var totalCommits = 0;

const AboutJSVersion = () => {
// GitLab API uses email for tracking commits, and username is used for tracking issues.
  const [KarenOneState, KarenOneUpdate] = useState({ email: 'karent@utexas.edu', username: 'krent22', commits: 0, tests: 0, issues: 0 });
  const [KarenTwoState, KarenTwoUpdate] = useState({ email: 'karen24603@gmail.com', username: 'Karen Tseng', commits: 0, tests: 0, issues: 0 });
  const [KarenThreeState, KarenThreeUpdate] = useState({ email: 'karen461907@gmail.com', username: 'KtNg52', commits: 0, tests: 0, issues: 0 });
  const [PoloOneState, PoloOneUpdate] = useState({ email: 'll34552@eid.utexas.edu', username: 'polol', commits: 0, tests: 0, issues: 0 });
  const [PoloTwoState, PoloTwoUpdate] = useState({ email: 'pololopeziv@gmail.com', username: 'lliv12', commits: 0, tests: 0, issues: 0 });
  const [PoloThreeState, PoloThreeUpdate] = useState({ email: 'pololopeziv@gmail.com', username: 'Polo_Lopez', commits: 0, tests: 0, issues: 0 })
  const [DonOneState, DonOneUpdate] = useState({ email: 'd.nguyen3235@gmail.com', username: 'dhn3235', commits: 0, tests: 0, issues: 0 });
  const [DonTwoState, DonTwoUpdate] = useState({ email: 'd.nguyen3235@gmail.com', username: 'TechDonut', commits: 0, tests: 0, issues: 0});
  const [JoelOneState, JoelOneUpdate] = useState({ email: 'jokerchan1st@gmail.com', username: 'JoelSetiawan', commits: 0, tests: 0, issues: 0 });
  const [JoelTwoState, JoelTwoUpdate] = useState({ email: 'joelsetiawan@Practice.localdomain', username: 'Joel Setiawan-Chong', commits: 0, tests: 0, issues: 0 });
  const [JosephOneState, JosephOneUpdate] = useState({ email: 'jtmartin@caltech.edu', username: 'Joseph Martinez', commits: 0, tests: 0, issues: 0 });
  const [JosephTwoState, JosephTwoUpdate] = useState({ email: 'jtmartin@caltech.edu', username: 'jtmartinez', commits: 0, tests: 0, issues: 0 });
  const [UnassignedState, UnassignedUpdate] = useState({ email: 'unassigned', username: 'unassigned', commits: 0, tests: 0, issues: 0 });
  const personArray = [[KarenOneState, KarenOneUpdate], [KarenTwoState, KarenTwoUpdate], 
    [KarenThreeState, KarenThreeUpdate], [PoloOneState, PoloOneUpdate], [PoloTwoState, PoloTwoUpdate], [PoloThreeState, PoloThreeUpdate], [DonOneState, DonOneUpdate],
    [DonTwoState, DonTwoUpdate], [JoelOneState, JoelOneUpdate], [JoelTwoState, JoelTwoUpdate], [JosephOneState, JosephOneUpdate], [JosephTwoState, JosephTwoUpdate], [UnassignedState, UnassignedUpdate]]

  //componentDidMount for Hooks
  useEffect(() => {

    //getting commit data
    axios.get('https://gitlab.com/api/v4/projects/39550052/repository/contributors')
      .then(res => {

        //for each member, find them in the array and their name from the api call and update their commits accordingly.
        res.data.forEach(member => {
          var email = member['email']; //gets the email
          var index = findMember(email);
          if (index != -1) { //found member, adding to their commits.
            personArray[index][1]({ ...personArray[index][0], commits: personArray[index][0].commits += member['commits'] });
            totalCommits += member['commits'];
          }
        })
      })

    //getting issues data
    personArray.forEach((member, index) => {
      axios.get('https://gitlab.com/api/v4/projects/39550052/issues_statistics?assignee_username=' + member[0]['username'])
        .then(res => { //getting call based on their username, then adding to their issues state.
          var issuesData = res.data;
          personArray[index][1]({ ...personArray[index][0], issues: personArray[index][0].issues += issuesData['statistics']['counts']['closed'] });
          totalIssues += issuesData['statistics']['counts']['closed'];
        })
    })

    axios.get('https://gitlab.com/api/v4/projects/39550052/issues_statistics?assignee_id=None')
        .then(res => { //getting call based on their username, then adding to their issues state.
          var issuesData = res.data;
          personArray[7][1]({ ...personArray[7][0], issues: personArray[7][0].issues += issuesData['statistics']['counts']['closed'] });
          totalIssues += issuesData['statistics']['counts']['closed'];
        })
    

  }, []); //second argument here so it only runs once (avoids infinite loop)


  // returns index of the new member for api call.
  const findMember = (email) => {
    for (var index = 0; index < personArray.length; index++) {
      var member = personArray[index][0];
      if (email === member['email']) {
        return index;
      }
    }
    return -1;
  }

  return (
    <div className="background-box">
      {/* outside-about-card for styling in css later. */}
      <Card className="about-us-card">
        <Card.Header as="h3">About Us</Card.Header>
        <Card.Body>
          <Card.Title>Purpose</Card.Title>
          <Card.Text>
            <p>
              ArtDB's purpose is to provide information on the works of many
              artists to highlight the differences and challenges faced that
              could be attributed to their gender, race, or status. 
            </p>
          </Card.Text>
          <Card.Title>Intended Users</Card.Title>
          <Card.Text>
            <p>We are
              an invaluable resource to users with an appreciation for art and
              are curious about the social climate within the industry.</p>
          </Card.Text>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">GitLab Stats</Card.Header>

        <Card.Body>
          <Card.Text>
            <p>Total Commits: {totalCommits}</p>
            <p>Total Issues: {totalIssues}</p>
            <p>Total Unit Tests: {61}</p>
          </Card.Text>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Body>
          <Card.Title as="h3">Our Team</Card.Title>
          <div className="member-flex">
            <Card className="member-card">
              <img 
                src= "./images/PoloPortrait.jpg" 
                height= "250"
                style={{width: "auto"}} 
                alt=""
              />
              <Card.Header as="h4">Polo Lopez</Card.Header>
              <Card.Body>
                <Card.Text>
                  <p>
                    <p>
                      {" "}
                      I'm a senior majoring in computer science. I was born in Dallas TX and raised in Austin. I like watching videos, reading the news and playing video games in my free time.{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Documentation and API Design</p>
                  <b>Total Commits: {PoloOneState.commits + PoloTwoState.commits}</b> <br />
                  <b>Total Issues: {PoloThreeState.issues}</b> <br />
                  <b>Total Unit Tests: {26}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
            <img 
                src= "./images/JosephPortrait.jpg" 
                height= "250"
                style={{width: "auto"}} 
                alt=""
              />
              <Card.Header as="h4">Joseph Martinez</Card.Header>
              <Card.Body>
                <Card.Text>
                  <p>
                    <p>
                      {" "}
                      I am a senior computer science major at UT Austin. I was born and raised in Spring, TX. I enjoy woodworking, grilling and smoking meats, and being disappointed by UT sport's teams.{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Front End</p>
                  <b>Total Commits: {JosephOneState.commits}</b> <br />
                  <b>Total Issues: {JosephTwoState.issues}</b> <br />
                  <b>Total Unit Tests: {0}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
              <img 
                src= "./images/DonPortrait.jpg" 
                height= "250"
                style={{width: "auto"}} 
                alt=""
              />
              <Card.Header as="h4">Don Nguyen</Card.Header>
              <Card.Body>
                <Card.Text>
                  <p>
                    <p>
                      {" "}
                      I am a third-year majoring in computer science at UT Austin. I was born in Arlington, TX but spent a good part of my childhood in Kansas City, Missouri. I like to play tennis or video games in my free time.{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>API Integration and Project Manager</p>
                 
                  <b>Total Commits: {DonOneState.commits}</b> <br />
                  <b>Total Issues: {DonTwoState.issues}</b> <br />
                  <b>Total Unit Tests: {16}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
              <img
                src= "./images/JoelPortrait.jpg"
                height= "250"
                style={{width: "auto"}}
                alt= ""
              />
              <Card.Header as="h4">Joel Setiawan Chong</Card.Header>
              <Card.Body>
                <Card.Text>
                  <p>
                    <p>
                      {" "}
                      I am a third-year computer science major at UT Austin. I was born and raised near Arlington, TX. I enjoy taking walks and reading books.{" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Front End</p>
                  <b>Total Commits: {JoelOneState.commits + JoelTwoState.commits}</b> <br />
                  <b>Total Issues: {JoelOneState.issues + JoelTwoState.issues}</b> <br />
                  <b>Total Unit Tests: {38}</b> 
                </Card.Text>
              </Card.Body>
            </Card>
            <Card className="member-card">
              <img
                src= "./images/KarenPortrait.jpg"
                height= "250"
                style={{width: "auto"}}
                alt= ""
              />
              <Card.Header as="h4">Karen Tseng</Card.Header>
              <Card.Body>
                <Card.Text>
                  <p>
                    <p>
                      {" "}
                      I am a third-year computer science major at UT Austin. I was born in California, but grew up in Katy, TX. In my free time, I love spending time with family and watching anime. {" "}
                    </p>
                  </p>
                  <b>Role:</b>  <p>Front End</p>
                  <b>Total Commits: {KarenOneState.commits + KarenTwoState.commits + KarenThreeState.commits}</b> <br />
                  <b>Total Issues: {KarenOneState.issues + KarenTwoState.issues + KarenThreeState.issues}</b> <br />
                  <b>Total Unit Tests: {16}</b>
                </Card.Text>
              </Card.Body>
            </Card>
          </div>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">Phase Leaders</Card.Header>
        <Card.Body>
          <Card.Text>
            <p>Phase 1: Don Nguyen</p>
            <p>Phase 2: Karen Tseng</p>
            <p>Phase 3: Polo Lopez</p>
            <p>Phase 4: Joel Setiawan Chong</p>
          </Card.Text>
        </Card.Body>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">
          APIs Used (Click the Picture to Go to API)
        </Card.Header>
        <div className="member-flex">
          <Card className="member-card">
            <h3>
              <a>Harvard Art Museum</a>
              <a href="https://github.com/harvardartmuseums/api-docs">
                <img
                  src="HarvardArtMuseum.jpg"
                  height="200"
                  width="300"
                  alt="Harvard Art Museum"
                />
              </a>
            </h3>
          </Card>

          <Card className="member-card">
            <h3>
              <a>Google Places</a>
              <a href="https://developers.google.com/maps/documentation/places/web-service/overview">
                <img
                  src="google-place.jpg"
                  height="200"
                  width="300"
                  alt="Google Places"
                />
              </a>
            </h3>
          </Card>

          <Card className="member-card">
            <h3>
              <a>WikiData SQL</a>
              <a href="https://query.wikidata.org/">
                <img
                  src="Wikidata_stamp.png"
                  height="200"
                  width="250"
                  alt="Wikidata"
                />
              </a>
            </h3>
          </Card>

          <Card className="member-card">
            <h3>
              <a href="https://documenter.getpostman.com/view/23525299/2s83tDpXqa">
                <img
                  src="vangogh.jpg"
                  height="200"
                  width="200"
                  alt="Wikidata"
                />
                ArtDB API
              </a>
            </h3>
            <Card.Text>
              <p>
                <b>
                  The Cleveland and Harvard APIs were scraped 
                  programatically in Python to obtain artwork information, 
                  artist information, and a list of museums. We then used Python to scrape
                  scrape Wikidata and the Google Places API for information about the 
                  museums on the list.
                </b>
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              <a>Cleveland Museum of Art</a>
              <a href="https://openaccess-api.clevelandart.org/">
                <img
                  src="cleveland.jpg"
                  height="200"
                  width="250"
                  alt="Cleveland"
                />
              </a>
            </h3>
          </Card>

          <Card className="member-card">
            <h3>
              <a>Mealmaker API</a>
              <a href="https://mealmaker.me/">
                <img
                  src="MEALMAKER.png"
                  height="200"
                  width="250"
                  alt="MealMaker"
                />
              </a>
            </h3>
          </Card>
        </div>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">Our Toolchain (Click the Picture to Go Tool Website)</Card.Header>
        <div className="member-flex">
          <Card className="member-card">
            <h3>
              <a> React </a>
              <a href="https://reactjs.org/">
                <img src="React192.png" height="200" width="200" alt="React Logo" />
              </a>
            </h3>
            
            <Card.Text>
              <p>
                A JavaScript library and framework that allowed for integration
                between the HTML and CSS of our About, Splash, Model and
                Instance Pages.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              <a> React Bootstrap </a>
              <a href="https://react-bootstrap.github.io/">
                <img
                  src="React Bootstrap.png"
                  height="200"
                  width="200"
                  alt="React Bootstrap"
                />
              </a>
            </h3>
          
            <Card.Text>
              <p>
                A web framework that provides common components to design a user
                interface. We used forms to allow users to search, cards to
                display details about artists, artworks and museums, and
                carousels to allow sliding through our models’ attributes.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              <a>GitLab</a>
              <a href="https://gitlab.com/">
                <img src="GitLab.png" height="200" width="200" alt="GitLab" />
              </a>
            </h3>
            <Card.Text>
              <p>
                A version control system that functions as a project management
                and development tool. The issues board GitLab provides helps us
                communicate and make progress. GitLab’s continuous integration
                and development pipelines help us build our application.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              <a>Amazon Web Services</a>
              <a href="https://aws.amazon.com/">
                <img src="AWS Precise.png" height="180" width="240" alt="AWS" /></a>{" "}
            </h3>
            
            <Card.Text>
              <p>
                Supports the server-side infrastructure of our web application.
                Used to connect our web application to a scalable server hosting
                website.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>Namecheap</a>
              <a href="https://www.namecheap.com/">
            <img src="Namecheap.png" height="180" width="280" alt="Namecheap" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                Obtained secure HTTP website domain name address from Namecheap.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a> Postman </a>
              <a href="https://www.postman.com/">
              <img src="PostMan.jpg" height="180" width="180" alt="Postman" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                Software that helps us document, test and design APIs. We
                created collections in Postman for each API we used and for each
                of our models.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>MaterialUI</a>
              <a href="https://mui.com/">
            <img src="MUI.png" height="180" width="180" alt="MaterialUI" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                A rich library full of beautiful reusable components. 
                We used its library for our search, filtering, and sorting bars, and its 
                tables, rows and columns. 
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>Jest</a>
              <a href="https://jestjs.io/">
            <img src="JEST.png" height="180" width="180" alt="Jest" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                A tool that was used to test front-end user interface components.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>Selenium</a>
              <a href="https://www.selenium.dev/">
            <img src="SELENIUM.png" height="180" width="180" alt="Selenium" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                Used to create automated front-end component tests and front-end to back-end component tests. 
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>Coolors.co</a>
              <a href="https://coolors.co/">
            <img src="COOLORS.png" height="180" width="180" alt="coolors" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                Used to enhance the website's color pallet and for improving front-end user interface components.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>React Simple Maps</a>
              <a href="https://www.react-simple-maps.io/">
            <img src="ReactSimpleMaps.png" height="180" width="180" alt="React Simple Maps" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                An API that was used to make our World Map and Texas Map for our Visualizations.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>SQL Alchemy</a>
              <a href="https://www.sqlalchemy.org/">
            <img src="SQLAlchemy.png" height="180" width="180" alt="SQLAlchemy" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                Used to connect our backend and database.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>PostgreSQL</a>
              <a href="https://www.postgresql.org/">
            <img src="PostgreSQL.svg" height="180" width="180" alt="PostgreSQL" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                Database management for supplying our API.
              </p>
            </Card.Text>
          </Card>

          <Card className="member-card">
            <h3>
              {" "}
              <a>Python Flask</a>
              <a href="https://flask.palletsprojects.com/en/2.2.x/">
            <img src="PythonFlask.png" height="180" width="180" alt="PythonFlask" />
              </a>{" "}
            </h3>

            <Card.Text>
              <p>
                An important tool used to connect front-end requests with our backend endpoints. 
              </p>
            </Card.Text>
          </Card>
        </div>
      </Card>

      <Card className="about-us-card">
        <Card.Header as="h3">Project Links</Card.Header>
        <h3>
          {" "}
          <a href="https://gitlab.com/krent22/idb">Project GitLab</a>{" "}
        </h3>

        <h3>
          {" "}
          <a href="https://documenter.getpostman.com/view/23525299/2s83tDpXqa">
            Postman API Documentation
          </a>{" "}
        </h3>
      </Card>
    </div>
  );
}

export default AboutJSVersion
