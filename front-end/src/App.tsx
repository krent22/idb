import "./App.css";
import React from "react";
import NavigationBar from "./components/NavigationBar";
import Splash from "./pages/Splash";


function App() {
  return (
    <div className="App">
      <NavigationBar />
      <Splash />
    </div>
  );
}

export default App;
