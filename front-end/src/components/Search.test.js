import React from "react";
import { render, screen } from "@testing-library/react";
import { describe, expect, test } from '@jest/globals';
import { SearchBar } from "./SearchBar";
// import { TextField } from "@mui/material"
import { BrowserRouter } from "react-router-dom";


describe('SearchBar', () => {
    test("SearchBar text rendering", () => {
        <BrowserRouter>
        render(<SearchBar />);
        </BrowserRouter>
    });
}); 

const artwork_search_text = ["Search sitewide", "Search", "sort", "Ascending", "filter on name", "filter on country_of_origin", "filter on category", "filter on artist_name", "filter on artist_gender"];
const artists_search_text = ["Search sitewide", "Search", "sort", "Ascending", "filter on name", "filter on birth_place", "filter on status", "filter on gender", "filter on ethnicity"];
const museums_search_text = ["Search sitewide", "Search", "sort", "Ascending", "filter on name", "filter on country", "filter on business_status"];

for (var i = 0; i < artwork_search_text.length; i++) {
    test("SearchBar ", () => {
        <BrowserRouter>
        render(<SearchBar />);
        expect(screen.getByDisplayValue(artwork_search_text[i])).toBeInTheDocument();
        </BrowserRouter>
    });
}

for (var i = 0; i < artists_search_text.length; i++) {
    test("SearchBar ", () => {
        <BrowserRouter>
        render(<SearchBar />);
        expect(screen.getByDisplayValue(artists_search_text[i])).toBeInTheDocument();
        </BrowserRouter>
    });
}

for (var i = 0; i < museums_search_text.length; i++) {
    test("SearchBar ", () => {
        <BrowserRouter>
            render(<SearchBar />);
            expect(screen.getByDisplayValue(museums_search_text[i])).toBeInTheDocument();
        </BrowserRouter>
    });
}