import React from "react";
import { render, screen } from "@testing-library/react";
import { describe, expect, test } from '@jest/globals';
import NavigationBar from "./NavigationBar";
import { BrowserRouter } from "react-router-dom";

const options = ["Artists", "Artworks", "Museums", "About"];

for (let i = 0; i < options.length; i++) {
  describe('Navbar', () => {
    test("Navbar text rendering", () => {
      <BrowserRouter>
      render(<NavigationBar />);
      expect(screen.getByText(options[i])).toBeInTheDocument();
      </BrowserRouter>
    });
  });
} 
