// imports for navbar from React-bootstrap sample code
import { Dropdown, DropdownButton } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./NavigationBar.css"
import { NavSearchBar } from "./NavSearchBar";
import { Link } from "react-router-dom";

function NavigationBar() {
  return (
    <Navbar bg="color" expand="lg">
      <Container>
        <Navbar.Brand href="/">ArtDB</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/artworks">Artworks</Nav.Link>
            <Nav.Link href="/artists">Artists</Nav.Link>
            <Nav.Link href="/museums">Museums</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Dropdown>
            <DropdownButton title={"Visualizations"} style={{ marginRight: "5px" }}>
              <Dropdown.Item className="background-gradient">
              <Link to ="/visuals"> Visualizations </Link>
              </Dropdown.Item>
              <Dropdown.Item className="background-gradient">
                <Link to ="/provider-visuals"> Provider Visualizations </Link>
              </Dropdown.Item>
            </DropdownButton>
            </Dropdown>
            <NavSearchBar/>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavigationBar;
