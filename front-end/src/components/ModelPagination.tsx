import { Col, Row } from 'react-bootstrap'
import Pagination from 'react-bootstrap/Pagination';
import { useMemo } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

import './ModelPagination.css'

export interface PaginationProps {
  totalCount: number;
  currentPage: number;
  pageSize: number;
  dataLoaded: boolean;
  searchParams: any;
  searchParamsChange: any;
}

// https://stackoverflow.com/questions/36947847/how-to-generate-range-of-numbers-from-0-to-n-in-es2015-only
const range = (start: number, end: number) => Array.from({ length: (end - start + 1) }, (v, k) => k + start);

// https://www.freecodecamp.org/news/build-a-custom-pagination-component-in-react/
function getRange(props: PaginationProps) {
  const totalPageCount = Math.ceil(props.totalCount / props.pageSize)
  const totalPageNumbers = 6

  if (totalPageNumbers > totalPageCount) {
    return range(1, totalPageCount)
  }

  const leftSiblingIndex = Math.max(props.currentPage - 1, 1);
  const rightSiblingIndex = Math.min(props.currentPage + 1, totalPageCount);

  const showLeftDots = leftSiblingIndex > 2;
  const showRightDots = rightSiblingIndex < totalPageCount - 2;

  if (showLeftDots && showRightDots) {
    let middleRange = range(leftSiblingIndex, rightSiblingIndex);
    return [1, '...', ...middleRange, '...', totalPageCount];
  }

  if (showRightDots && !showLeftDots) {
    let leftRange = range(1, 5);

    return [...leftRange, '...', totalPageCount];
  }


  if (showLeftDots && !showRightDots) {
    let rightRange = range(totalPageCount - 4, totalPageCount);
    return [1, '...', ...rightRange];
  }
}

export function PageControl(props: PaginationProps) {

  const paginationRange = useMemo(() => {
    return getRange(props)
  }, [props])

  const pageRange = useMemo(() => {
    return range(1, Math.ceil(props.totalCount / props.pageSize))
  }, [props])

  const sizeRange = [10, 20, 30, 50, 100]

  if (!props.dataLoaded) {
    return (
      <div></div>
    )
  } else {
    const nextPage = () => {
      changePage(props.currentPage + 1);
    }

    const prevPage = () => {
      changePage(props.currentPage - 1);
    }

    const changePage = (page: number) => {
      let newParams = props.searchParams
      newParams.delete("page")
      newParams.append("page", page.toString())

      props.searchParamsChange(newParams)
    }

    const changePageSize = (size: number) => {
      let newParams = props.searchParams
      newParams.delete("per_page")
      newParams.append("per_page", size.toString())

      // Handle going back if not elements to show
      if (size * props.currentPage >= props.totalCount) {
        changePage(1)
      }

      props.searchParamsChange(newParams)
    }


    return (
      <div>
        <Pagination>
          <Pagination.Prev
            disabled={props.currentPage === 1}
            onClick={prevPage} />

          {paginationRange?.map(page => {
            if (typeof page === 'string') {
              return <Pagination.Ellipsis />
            } else {
              return (
                <div className="pagination-item" key={page}>
                  <Pagination.Item
                    active={page === props.currentPage}
                    // TODO: make to link to new page with search terms changed
                    // https://ultimatecourses.com/blog/navigate-to-url-query-strings-search-params-react-router
                    onClick={() => changePage(page)}>
                    {page}
                  </Pagination.Item>
                </div>
              )
            }
          })}

          <Pagination.Next
            disabled={props.currentPage === Math.ceil(props.totalCount / props.pageSize)}
            onClick={nextPage} />
        </Pagination>
        <div className='dropdowns'>
          <Row >
            <Col>
              <Dropdown className='dropdownMenu'>
                <Dropdown.Toggle id="dropdown-basic">
                  Page size
                </Dropdown.Toggle>
                <Dropdown.Menu flip={true}>
                  {sizeRange?.map(size => {
                    return (
                      <div className="size-select-item" key={size}>
                        <Dropdown.Item
                          onClick={() => changePageSize(size)}>
                          {size}
                        </Dropdown.Item>
                      </div>
                    )
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown className='dropdownMenu'>
                <Dropdown.Toggle id="dropdown-basic">
                  Page
                </Dropdown.Toggle>
                <Dropdown.Menu flip={true}>
                  {pageRange?.map(page => {
                    return (
                      <div className="page-select-item" key={page}>
                        <Dropdown.Item
                          onClick={() => changePage(page)}>
                          {page}
                        </Dropdown.Item>
                      </div>
                    )
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </Col>
          </Row>
        </div>
        <Row className="resultsCount">
          Total Results: {props.totalCount}
        </Row>
      </div>
    );
  }
}
