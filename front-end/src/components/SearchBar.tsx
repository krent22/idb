import TextField from "@mui/material/TextField";
import { Select, MenuItem, InputLabel, FormControl, SelectChangeEvent } from "@mui/material";
import { Card, Row, Col } from "react-bootstrap";
import { useState, } from 'react';
import { FilterBar } from "./FilterBar";

export interface SearchProps {
  searchParams: any;
  searchParamsChange: any;
  sortableFields: any;
  filterData: any;
}

export function SearchBar(props: SearchProps) {

  const [inputText, setInputText] = useState(props.searchParams.get("query") ?? "");

  const [sort, setSort] = useState(props.searchParams.get("sort")?.split("=")[0] ?? "")
  const [direction, setSortDirection] = useState(props.searchParams.get("sort")?.split("=")[1] ?? "ascending")

  const updateParam = (param: string, value: string) => {
    let newParams = props.searchParams

    newParams.delete(param)
    if (value !== "") {
      newParams.append(param, value)
    }
    props.searchParamsChange(newParams)
  }


  const updateSort = (e: SelectChangeEvent) => {
    setSort(e.target.value)
    updateParam("sort", e.target.value + "=" + direction)
  }

  const updateSortDirection = (e: SelectChangeEvent) => {
    setSortDirection(e.target.value)
    updateParam("sort", sort + "=" + e.target.value)
  }

  const search = (e: any) => {
    console.log(e.target.value)
    setInputText(e.target.value)
      if (inputText.length >= 0) {
        updateParam("query", e.target.value);
      }
    
  }

  return (
    // https://dev.to/salehmubashar/search-bar-in-react-js-545l
    <Card style={{ backgroundImage: "linear-gradient(#a5efff, #90E0EF)", border: "#a5efff" }}>
      <Row className="searchSortRow" key="searchRow">
        <Col key = "searchBar">
          <Row>
            <Col>
              <TextField
                id="outlined-basic"
                variant="outlined"
                fullWidth
                value={inputText}
                label="Search"
                onChange={search}
                // https://stackoverflow.com/questions/46966413/how-to-style-material-ui-textfield
                InputProps={{ style: { backgroundColor: "white" } }}
                style={{ backgroundColor: "#a5efff" }}
              />
            </Col>
          </Row>
        </Col>
        <Col key="sorting">
          <Row>
            <Col key="sortBy">
              <FormControl fullWidth>
                <InputLabel id="sort-select-label">Sort</InputLabel>
                <Select
                  labelId="sort-select-label"
                  id="sort-select"
                  value={sort}
                  label="sort"
                  onChange={updateSort} 
                  style = {{ backgroundColor: "white" }}>
                  {props.sortableFields.map((item: string) => {
                    return (
                      <MenuItem value={item} key = {item}>{item.replaceAll("_", " ")}</MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Col>
            <Col key="sortDirection">
              <FormControl fullWidth>
                <InputLabel id="sort-direction-label">Direction</InputLabel>
                <Select
                  labelId="sort-direction-label"
                  id="sort-direction-select"
                  value={direction}
                  label="sortDirection"
                  disabled={sort === ""}
                  onChange={updateSortDirection}
                  style = {{ backgroundColor: "white" }} >
                  <MenuItem value={"ascending"}>Ascending</MenuItem>
                  <MenuItem value={"descending"}>Descending</MenuItem>
                </Select>
              </FormControl>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row style={{ height: "30px" }}/>
      <Row>
      <FilterBar
        searchParams={props.searchParams}
        searchParamsChange={props.searchParamsChange}
        filterableFieldsAndValues={props.filterData} />
      </Row>
      <Row style={{ height: "30px" }}/>
    </Card>
  )
}