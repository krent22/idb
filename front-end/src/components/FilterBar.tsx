import { Row, Col } from "react-bootstrap";
import { FilterField } from "./FilterField";

export interface FilterProps {
  searchParams: any;
  searchParamsChange: any;
  filterableFieldsAndValues: any;
}

export function FilterBar(props: FilterProps) {
  return (
    <Row className="filterRow" key="filterRow">
      {props.filterableFieldsAndValues.map((item: any) => {
        return (
          <Col>

            <FilterField
              name={item.name}
              displayName={item.display_name}
              values={item.values}
              searchParams={props.searchParams}
              searchParamsChange={props.searchParamsChange} />
          </Col>
        );
      })}
    </Row>
  )
}