import { Select, MenuItem, InputLabel, FormControl, SelectChangeEvent } from "@mui/material";
import { useState, } from 'react';

export interface FilterFieldProps {
  name: string;
  values: any
  displayName: string
  searchParams: any
  searchParamsChange: any
}

export function FilterField(props: FilterFieldProps) {

  const [value, setValue] = useState(props.searchParams.get(props.name) ?? "");

  const updateParam = (param: string, value: string) => {
    let newParams = props.searchParams

    newParams.delete(param)
    if (value !== "") {
      newParams.append(param, value)
    }
    props.searchParamsChange(newParams)
  }

  const updateValue = (e: SelectChangeEvent) => {
    setValue(e.target.value)
    updateParam(props.name, e.target.value)
  }

  return (
    <FormControl fullWidth>
      <InputLabel id="select-label">{"Filter on " + props.displayName}</InputLabel>
      <Select
        labelId="select-label"
        id="select"
        value={value}
        label={"Filter on " + props.displayName}
        onChange={updateValue} 
        style = {{ backgroundColor: "white" }}>
        {props.values.map((item: any) => {
          return (
            <MenuItem value={item} key={item}>{item}</MenuItem>
          );
        })}
      </Select>
    </FormControl>
  )
}