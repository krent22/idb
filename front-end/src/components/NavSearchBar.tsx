import { autocompleteClasses } from "@mui/material";
import TextField from "@mui/material/TextField";
import { maxHeaderSize } from "http";
import { mainModule } from "process";
import { useState, } from 'react';
import { useNavigate, } from 'react-router-dom';
import MuseumInstance from "../pages/instances/MuseumInstance";

export interface SearchProps {
  searchParams: any;
  searchParamsChange: any;
  sortableFields: any;
  filterableFields: any;
}

export function NavSearchBar() {

  const navigate = useNavigate()

  const [inputText, setInputText] = useState("");


  const search = (e: any) => {
    if (e.keyCode === 13) {
      if (inputText.length >= 0) {
        setInputText("")
        navigate(`/search?query=${inputText}`)
        navigate(0)
      }
    }
  }

  return (
    <TextField
      size="small"
      id="outlined-basic"
      variant="outlined"
      style = {{width: 'min(100vw, 800px)', height: 40}}
      value={inputText}
      label="Search sitewide"
      onChange={(e: any) => {
        setInputText(e.target.value);
      }}
      InputProps={{ style: { backgroundColor: "white" } }}
      onKeyDown={search}
    />
  )
}