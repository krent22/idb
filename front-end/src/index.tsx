import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import NavigationBar from "./components/NavigationBar";
import Splash from "./pages/Splash";
import About from "./pages/About";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ArtworkGrid from "./pages/models/ArtworkGrid";
import ArtistGrid from "./pages/models/ArtistTable";
import MuseumGrid from "./pages/models/MuseumTable";
import ArtistInstance from "./pages/instances/ArtistInstance";
import ArtworkInstance from "./pages/instances/ArtworkInstance";
import MuseumInstance from "./pages/instances/MuseumInstance";
import Search from "./pages/AllModelSearch";
import AboutJSVer from "./pages/AboutJSVer";
import reportWebVitals from "./reportWebVitals";
import VisualizationData from "./pages/visualizations/pages/VisualizationData";
import ProviderVisualizationData from "./pages/visualizations/pages/ProviderVisualizationData";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  // Router code from AnimalWatch Sp22
  <BrowserRouter>
    <NavigationBar />
    {/* Routes renders only used pages. */}
    <Routes>
      {/* Exact needed to prevent users from reaching splash pages each time they click on
        a new path. */}
      <Route path="/" element={<Splash />} />
      <Route path="/about" element={<AboutJSVer />} />

      <Route path="/artworks" element={<ArtworkGrid />} />
      <Route path="/artists" element={<ArtistGrid />} />
      <Route path="/museums" element={<MuseumGrid />} />

      <Route path="/artists/:id" element={<ArtistInstance/>} />
      <Route path="/artworks/:id" element={<ArtworkInstance/>} />
      <Route path="/museums/:id" element={<MuseumInstance/>} />
      <Route path="/visuals" element={<VisualizationData/>} />
      <Route path="/provider-visuals" element={<ProviderVisualizationData/>} />
      <Route path="/search" element={<Search/>} />


    </Routes>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
