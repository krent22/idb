import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";
import { BrowserRouter } from 'react-router-dom';

test("Checks Splash Page for elements", () => {
  <BrowserRouter>
  render(<App />);
  const linkElement = screen.getByText("ArtDB");
  expect(linkElement).toBeInTheDocument();
  </BrowserRouter>
});
