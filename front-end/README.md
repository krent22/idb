# Directions for Developing on Front End

### First Time Setup Instructions

1. Install Docker Desktop

1. Pull the krent2502/idb-frontend-v1 Docker image with the command:

    `docker pull krent2502/idb-frontend-v1`

### Recurring Setup Instructions
1. Open Docker Desktop. If you do not have Docker Desktop running, docker will present the error message:

    `docker: error during connect: This error may indicate that the docker daemon is not running`

    when you try to start the container.

1. Start the krent2502/idb-frontend-v1 image in the front-end directory.
    - If you are using Windows, this can be done in the _command prompt_ terminal with the command:

        `docker run -it --rm -v %cd%:/app -v /app/node_modules -p 3000:3000 -e WATCHPACK_POLLING=true krent2502/idb-frontend-v1`

    - If you are using Windows Powershell, use the command:

        `docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3000:3000 -e WATCHPACK_POLLING=true krent2502/idb-frontend-v1`
    
    - In general, the command should be:

        `docker run -it --rm -v <current_working_directory>:/app -v /app/node_modules -p 3000:3000 -e WATCHPACK_POLLING=true krent2502/idb-frontend-v1`

    NOTE: The -e WATCHPACK_POLLING=true is required if you want your code changes to be reflected in the react app that the docker container displays.

1. Go to localhost:3000/ to see how your code changes render.

- Note: If your code change requires a new package, a new Docker image containing the package will need to be built. To build a new docker image, cd into the front-end directory and run the command:

    `docker build -t <your docker username>/<image name> .`

    After the image has been successfully created, share the new image name with the group and update this README with the new image name.
    
### Deploying to Dev
1. To deploy to the [dev website](https://front-end-dev.d2414og9qxwy80.amplifyapp.com/home), push your code changes to the front-end-dev branch of the IDB repository. The GitLab pipeline is linked with AWS Amplify and will deploy the changes for you.

### Deploying to Prod
1. It is **NOT** recommended to push to the master branch. Development should take place in the front-end-dev branch. Once the previous phase has been graded and the group has decided that front-end looks okay, we will merge the front-end-dev branch into the master branch. Once the merge is complete, GitLab pipelines and AWS Amplify will deploy all changes for us.
