# from GetThatBread repository: https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/blob/main/frontend/getthatbread/selenium/test_gui.py

import sys
import time
import pytest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = None
wait = None
local = False # set to FALSE when pushing to gitlab
dev = False # set to FALSE to test actual webpage

url = "https://www.artdb.me/"
if dev:
    url = "https://front-end-dev.d2414og9qxwy80.amplifyapp.com/"

def setup_module():
    print("beginning setup for test_gui module")

    # allow gitlab ci/cd to run selenium tests
    global driver, wait
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("window-size=1200x600")
    if local:
        driver = webdriver.Chrome(service = Service(ChromeDriverManager().install()), options = chrome_options)
    else:
        driver = Remote(
            "http://selenium__standalone-chrome:4444/wd/hub",
            desired_capabilities=chrome_options.to_capabilities(),
        )
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    return driver

def teardown_module():
    print("tearing down test_gui module")
    driver.quit()

'''
Basic Tests
'''
def test_title():
    print("starting test_title")
    assert driver.title == "ArtDB"

'''
Navbar Tests
'''
def test_navbar_brand():
    print("starting test_navbar_brand")
    home = driver.find_elements(By.XPATH, "/html/body/div/nav/div/a")
    home[0].click()
    assert driver.current_url == url

def test_navbar_sitewide_search():
    print("starting test_navbar_sitewide_search")
    try:
        # WebDriverWait(self.driver, 10).until(
        EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/nav/div/div/div/div'))
        # )
    except Exception as ex:
        print("Couldn't find search bar: " + str(ex))


def test_navbar_artworks():
    print("starting test_navbar_artworks")
    artworks = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[1]")
    artworks.click()
    assert driver.current_url == url + "artworks"

def test_navbar_artists():
    print("starting test_navbar_artists")
    artists = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[2]")
    artists.click()
    assert driver.current_url == url + "artists"

def test_navbar_museums():
    print("starting test_navbar_museums")
    museums = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[3]")
    museums.click()
    assert driver.current_url == url + "museums"

def test_navbar_about():
    print("starting test_navbar_about")
    about = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[4]")
    about.click()
    assert driver.current_url == url + "about"

'''
About Page Tests
'''
def test_about_page_title():
    print("starting test_about_page_title")
    driver.get(url + 'about')
    title = driver.find_element(By.XPATH, "/html/body/div/div/div[1]/h3")
    assert title.text == "About Us"

def test_about_page_stats_title():
    print("starting test_about_page_stats_title")
    driver.get(url + 'about')
    title = driver.find_element(By.XPATH, "/html/body/div/div/div[2]/h3")
    assert title.text == "GitLab Stats"

def test_about_page_stats_body():
    print("starting test_about_page_stats_body")
    driver.get(url + 'about')
    path = "/html/body/div/div/div[2]/div/p/"
    commits = driver.find_element(By.XPATH, path + "p[1]")
    issues = driver.find_element(By.XPATH, path + "p[2]")
    unit_tests = driver.find_element(By.XPATH, path + "p[3]")
    assert "Total Commits: " in commits.text
    assert "Total Issues: " in issues.text
    assert "Total Unit Tests: " in unit_tests.text

def test_about_page_members():
    print("starting test_about_page_members")
    driver.get(url + 'about')
    path = "/html/body/div/div/div[3]/div/div/div[{}]/h4"
    members = ["Polo Lopez", "Joseph Martinez", "Don Nguyen", "Joel Setiawan Chong", "Karen Tseng"]
    for i in range(1,6): 
        name = driver.find_element(By.XPATH, path.format(i))
        assert name.text == members[i - 1]

'''
Artworks Page Tests
'''
def test_artwork_title():
    print("starting test_artwork_title")
    driver.get(url + 'artworks')
    count = driver.find_element(By.XPATH, "html/body/div/div/div[1]")
    assert count.text == "Artworks"

def test_artwork_page_count():
    print("starting test_artwork_page_count")
    driver.get(url + 'artworks')
    driver.implicitly_wait(5)
    count = driver.find_element(By.XPATH, "html/body/div/div/div[6]/div[2]")
    assert "Total Results: " in count.text

def test_artwork_search_bar():
    print("starting test_artwork_search_bar")
    driver.get(url + 'artworks')
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[1]/div[1]/div/div/div/label")
    assert count.text == "Search"

def test_artwork_filters():
    print("starting test_artwork_filters")
    driver.get(url + 'artworks')

    # test name filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div/div[1]/div/label")
    assert count.text == "Filter on artist gender"

    # test country of origin filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[2]/div/label")
    assert count.text == "Filter on category"

    # test category filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[3]/div/label")
    assert count.text == "Filter on origin country"

    # test artist name filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[4]/div/label")
    assert count.text == "Filter on type"

def test_artwork_sort():
    print("starting test_artwork_sort")
    driver.get(url + 'artworks')
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[1]/div[2]/div/div[1]/div/label")
    assert count.text == "Sort"


'''
Artists Page Tests
'''
def test_artist_title():
    print("starting test_artist_title")
    driver.get(url + 'artists')
    count = driver.find_element(By.XPATH, "html/body/div/div/div[1]")
    assert count.text == "Artists"

def test_artist_page_count():
    print("starting test_artist_page_count")
    driver.get(url + 'artists')
    driver.implicitly_wait(5)
    count = driver.find_element(By.XPATH, "html/body/div/div/div[4]/div[2]")
    assert "Total Results: " in count.text

def test_artist_search_bar():
    print("starting test_artist_search_bar")
    driver.get(url + 'artists')
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[1]/div[1]/div/div/div/label")
    assert count.text == "Search"

def test_artist_filters():
    print("starting test_artist_filters")
    driver.get(url + 'artists')

    # test name filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[1]/div/label")
    assert count.text == "Filter on birth place"

    # test birth place filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[2]/div/label")
    assert count.text == "Filter on ethnicity"

    # test status filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[3]/div/label")
    assert count.text == "Filter on gender"

    # test gender filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[4]/div/label")
    assert count.text == "Filter on status"


def test_artist_sort():
    print("starting test_artist_sort")
    driver.get(url + 'artists')
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[1]/div[2]/div/div[1]/div/label")
    assert count.text == "Sort"

'''
Museums Page Tests
'''
def test_museum_title():
    print("starting test_museum_title")
    driver.get(url + 'museums')
    count = driver.find_element(By.XPATH, "html/body/div/div/div[1]")
    assert count.text == "Museums"

def test_museum_page_count():
    print("starting test_museum_page_count")
    driver.get(url + 'museums')
    driver.implicitly_wait(5)
    count = driver.find_element(By.XPATH, "html/body/div/div/div[4]/div[2]")
    assert "Total Results: " in count.text

def test_museum_search_bar():
    print("starting test_museum_search_bar")
    driver.get(url + 'museums')
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[1]/div[1]/div/div/div/label")
    assert count.text == "Search"

def test_museum_filters():
    print("starting test_museum_filters")
    driver.get(url + 'museums')

    # test name filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[1]/div/label")
    assert count.text == "Filter on status"

    # test country filter
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[3]/div[1]/div[2]/div/label")
    assert count.text == "Filter on country"

def test_museum_sort():
    print("starting test_museum_sort")
    driver.get(url + 'museums')
    count = driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div[1]/div[2]/div/div[1]/div/label")
    assert count.text == "Sort"
