.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# All of these make commands must be called in root directory

# run docker
frontend-docker:
	cd front-end
	docker run -it --rm -v %cd%:/app -v /app/node_modules -p 3000:3000 -e WATCHPACK_POLLING=true krent2502/idb-frontend-v1

frontend-docker-build:
	cd front-end
	docker build -t krent2502/idb-frontend .

backend-docker:
	docker run --rm -it -p 5000:5000 -v %cd%:/usr/python -w /usr/python krent2502/idb-beanstalk-backend

all:

# auto format the code
format:
	black ./backend/*.py

install:
	pip install -r ./backend/requirements.txt

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        

# check the existence of check files
check: $(CFILES)

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__

python-tests:
	python3 backend/tests.py
