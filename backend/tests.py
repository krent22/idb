import requests
from unittest import main, TestCase


# Test Code follows same structure as GalleryDB/backend/tests.py

ENDPOINT = "https://api.artdb.me"


class Tests(TestCase):

    # Artist endpoint tests

    # Test the first entry from artists page
    def test_artists(self):
        artists = requests.get(f"{ENDPOINT}/artists")
        assert artists.status_code == 200
        data = artists.json()

        # test the first entry
        artist_entry = data["data"][0]
        bio_snippet = "an amateur painter"

        # schema for artists below
        assert artist_entry["name"] == "Albert Besnard"
        assert artist_entry["gender"] == "male"
        assert artist_entry["birth_place"] == "Paris, France"
        assert artist_entry["status"] == "deceased"
        assert bio_snippet in artist_entry["biography"]
        assert artist_entry["ethnicity"] == "French"
        assert artist_entry["birth_date"] == 1849
        assert artist_entry["death_date"] == 1934
        assert artist_entry["number_of_works"] == 19
        assert artist_entry["other_names"] == None

    # test artist instance
    def test_artist_instance(self):
        artist = requests.get(f"{ENDPOINT}/artists/2")
        assert artist.status_code == 200
        data = artist.json()
        artist_entry = data["data"]
        # test content of artist entry from id
        bio_snippet = "Ferdinand Georg Waldmüller was the leading"

        # schema below
        assert artist_entry["name"] == "Ferdinand Georg Waldmüller"
        assert artist_entry["gender"] == "male"
        assert artist_entry["birth_place"] == "Vienna, Austria"
        assert artist_entry["status"] == "deceased"
        assert artist_entry["ethnicity"] == "Austrian"
        assert artist_entry["birth_date"] == 1793
        assert artist_entry["death_date"] == 1865
        assert artist_entry["number_of_works"] == 2
        assert bio_snippet in artist_entry["biography"]
        assert artist_entry["other_names"] == None

    # test artwork endpoints

    # test artwork from all artworks
    def test_artworks(self):
        artworks = requests.get(f"{ENDPOINT}/artworks")
        assert artworks.status_code == 200
        data = artworks.json()

        # test artwork entry by index
        artwork_entry = data["data"][0]

        # schema below
        assert artwork_entry["name"] == "A Brook at Le puits noir, near Ornans"
        assert artwork_entry["artist_name"] == "Gustave Courbet"
        assert artwork_entry["artist_id"] == 11
        assert artwork_entry["owning_museum_id"] == None
        assert artwork_entry["country_of_origin"] == "French"
        assert artwork_entry["type"] == None
        assert artwork_entry["category"] == "European and American Art"
        assert artwork_entry["date_of_creation"] == 1864
        assert artwork_entry["acquisition"] == "Harvard Art Museums/Fogg Museum, Bequest of Grenville L. Winthrop"
        assert artwork_entry["location"] == "Gallery 2100, Harvard Art Museum"
        assert artwork_entry["medium"] == "Oil on canvas"
        assert (
            artwork_entry["dimensions"]
            == "55 x 89 cm (21 5/8 x 35 1/16 in.)\r\nframed: 79.4 x 113 x 8.9 cm (31 1/4 x 44 1/2 x 3 1/2 in.)"
        )
        assert (
            artwork_entry["image_url"]
            == "https://nrs.harvard.edu/urn-3:HUAM:DDC231388_dynmc"
        )

    def test_artwork_instance(self):
        artworkResponse = requests.get(f"{ENDPOINT}/artworks/0")
        assert artworkResponse.status_code == 200
        data = artworkResponse.json()
        artwork_entry = data["data"]

        # schema below
        assert artwork_entry["name"] == "Parsifal"
        assert artwork_entry["artist_name"] == "Odilon Redon"
        assert artwork_entry["artist_id"] == 0
        assert artwork_entry["owning_museum_id"] == 3
        assert artwork_entry["country_of_origin"] == "France"
        assert artwork_entry["type"] == "Print"
        assert artwork_entry["category"] == "Prints"
        assert artwork_entry["date_of_creation"] == 1892
        assert artwork_entry["acquisition"] == "Gift of Ralph King"
        assert artwork_entry["location"] == None
        assert artwork_entry["medium"] == "lithograph"
        assert (
            artwork_entry["dimensions"]
            == "Image: 32.2 x 24.2 cm (12 11/16 x 9 1/2 in.)"
        )
        assert (
            artwork_entry["image_url"]
            == "https://openaccess-cdn.clevelandart.org/1925.21/1925.21_web.jpg"
        )

    # test museum endpoints
    def test_museums(self):
        museumsResponse = requests.get(f"{ENDPOINT}/museums")
        assert museumsResponse.status_code == 200
        data = museumsResponse.json()
        museum_entry = data["data"][1]

        # schema below
        assert museum_entry["name"] == "Ackland Art Museum"
        assert (
            museum_entry["address"] == "101 S Columbia St, Chapel Hill, NC 27599, USA"
        )
        assert museum_entry["business_status"] == "OPERATIONAL"
        assert museum_entry["collection_size"] == None
        assert museum_entry["country"] == "United States of America"
        assert museum_entry["date_established"] == "2008"
        assert museum_entry["director"] == None
        assert museum_entry["founder"] == None
        assert (
            museum_entry["image_exterior"]
            == "https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Ackland_Art_Museum.jpg"
        )
        assert museum_entry["image_interior"] == None
        assert museum_entry["latitude"] == 35.912593
        assert museum_entry["longitude"] == -79.054867
        assert museum_entry["logo"] == None
        assert museum_entry["phone_number"] == None
        assert museum_entry["rating"] == 4.6
        assert museum_entry["utc_offset"] == -240
        assert museum_entry["visitors_per_year"] == None
        assert museum_entry["website"] == "http://www.ackland.org/"

    def test_museum_instance(self):
        museumResponse = requests.get(f"{ENDPOINT}/museums/1")
        assert museumResponse.status_code == 200
        data = museumResponse.json()
        museum_entry = data["data"]

        # schema below
        assert museum_entry["name"] == "Musée d'Art Moderne"
        assert (
            museum_entry["address"] == "11 Av. du Président Wilson, 75116 Paris, France"
        )
        assert museum_entry["business_status"] == "OPERATIONAL"
        assert museum_entry["collection_size"] == None
        assert museum_entry["country"] == "France"
        assert museum_entry["date_established"] == "1935"
        assert museum_entry["director"] == None
        assert museum_entry["founder"] == None
        assert (
            museum_entry["image_exterior"]
            == "https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Musée_d'Art_moderne_de_la_Ville_de_Paris.jpg"
        )
        assert (
            museum_entry["image_interior"]
            == "https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Jean_Metzinger,_L'Oiseau_Bleu_(left),_André_Lhote,_two_works_(center),_Albert_Gleizes,_Baigneuse_(right),_Musée_d'Art_Moderne_de_la_Ville_de_Paris.jpg"
        )
        assert museum_entry["latitude"] == 48.8643421
        assert museum_entry["longitude"] == 2.2978208
        assert museum_entry["logo"] == None
        assert museum_entry["phone_number"] == None
        assert museum_entry["rating"] == 4.4
        assert museum_entry["utc_offset"] == 120
        assert museum_entry["visitors_per_year"] == 800000
        assert museum_entry["website"] == "http://www.mam.paris.fr/"


if __name__ == "__main__":
    main()
