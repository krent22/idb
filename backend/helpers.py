from sqlalchemy import and_, or_, cast, String, nullslast, Integer
from sqlalchemy.sql.expression import desc


def search(exec_stmt, model, terms, sort_relevance, subq):
    # code to sort by relevance is from https://gitlab.com/maxwthomas/cs373-idb/-/blob/main/backend/app.py
    search_filter_exprs = [
        (
            and_(
                model.__getattribute__(model, att) != None,
                cast(model.__getattribute__(model, att), String).ilike(f"%{ term }%"),
            )
        ).cast(Integer)
        for term in terms
        for att in model.search_fields
    ]
    search_filter_exprs.append(model.id.in_(subq).cast(Integer) * 0.5)
    search_weight = sum(search_filter_exprs)
    exec_stmt = exec_stmt.filter(search_weight != 0)
    if sort_relevance:
        exec_stmt = exec_stmt.order_by(desc(search_weight))

    return exec_stmt


def sort(exec_stmt, model, sort):
    attribute, direction = sort.split("=")
    if hasattr(model, attribute) and direction == "ascending":
        exec_stmt = exec_stmt.order_by(
            nullslast(model.__getattribute__(model, attribute))
        )
    elif hasattr(model, attribute) and direction == "descending":
        exec_stmt = exec_stmt.order_by(
            nullslast(model.__getattribute__(model, attribute).desc())
        )
    return exec_stmt


def filter(exec_stmt, model, params):
    if params:
        exec_stmt = exec_stmt.where(
            and_(
                model.__getattribute__(model, p) == v
                for p, v in params.items()
                if hasattr(model, p) and p != "query"
            )
        )
    return exec_stmt
