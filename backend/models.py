from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
CORS(app)

database_url = (
    "postgresql+psycopg2://{USERNAME}:{PASSWORD}@{HOSTNAME}:{PORT}/{DB_NAME}".format(
        USERNAME="group11_2",
        PASSWORD="ArtDB22swe",
        HOSTNAME="artdbinstance.czk8e1ewixri.us-east-2.rds.amazonaws.com",
        PORT=5432,
        DB_NAME="artdbinstance",
    )
)

app.config["SQLALCHEMY_DATABASE_URI"] = database_url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


class Artist(db.Model):
    __tablename__ = "artist"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    gender = db.Column(db.String())
    biography = db.Column(db.String())
    ethnicity = db.Column(db.String())
    birth_date = db.Column(db.Integer())
    death_date = db.Column(db.Integer())
    status = db.Column(db.String())
    number_of_works = db.Column(db.Integer())
    other_names = db.Column(db.ARRAY(db.String()))
    birth_place = db.Column(db.String())
    image = db.Column(db.String())

    search_fields = [
        "name",
        "gender",
        "biography",
        "ethnicity",
        "birth_date",
        "death_date",
        "number_of_works",
        "other_names",
        "status",
        "birth_place",
    ]


class Artwork(db.Model):
    __tablename__ = "artwork"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    country_of_origin = db.Column(db.String())
    date_of_creation = db.Column(db.Integer())
    period = db.Column(db.String())
    category = db.Column(db.String())
    type = db.Column(db.String())
    artist_name = db.Column(db.String())
    artist_id = db.Column(db.Integer())
    artist_gender = db.Column(db.String())
    owning_museum = db.Column(db.String())
    owning_museum_id = db.Column(db.Integer())
    location = db.Column(db.String())
    dimensions = db.Column(db.String())
    acquisition = db.Column(db.String())
    medium = db.Column(db.String())
    description = db.Column(db.String())
    image_url = db.Column(db.String())

    search_fields = [
        "name",
        "country_of_origin",
        "category",
        "type",
        "artist_name",
        "artist_gender",
        "owning_museum",
        "location",
        "dimensions",
        "acquisition",
        "medium",
        "description",
        "date_of_creation",
    ]


class Museum(db.Model):
    __tablename__ = "museum"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    country = db.Column(db.String())
    visitors_per_year = db.Column(db.Integer())
    date_established = db.Column(db.String())
    founder = db.Column(db.String())
    director = db.Column(db.String())
    collection_size = db.Column(db.Integer())
    phone_number = db.Column(db.String())
    address = db.Column(db.String())
    website = db.Column(db.String())
    business_status = db.Column(db.String())
    rating = db.Column(db.Float())
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    utc_offset = db.Column(db.Integer())
    logo = db.Column(db.String())
    image_exterior = db.Column(db.String())
    image_interior = db.Column(db.String())

    search_fields = [
        "name",
        "country",
        "visitors_per_year",
        "date_established",
        "founder",
        "director",
        "collection_size",
        "phone_number",
        "address",
        "website",
        "business_status",
        "rating",
        "latitude",
        "longitude",
        "utc_offset",
    ]


class Exhibition(db.Model):
    __tablename__ = "exhibition"
    id = db.Column(db.Integer(), primary_key=True)
    artwork_id = db.Column(db.Integer())
    artist_id = db.Column(db.Integer())
    museum_id = db.Column(db.Integer())
    exhibition_date = db.Column(db.String())
