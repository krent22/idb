import os
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
CORS(app)

"""
RDS CONFIG
"""
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://{RDS_USERNAME}:{RDS_PASSWORD}@{RDS_HOSTNAME}:{RDS_PORT}/{RDS_DB_NAME}".format(
    RDS_USERNAME="group11_2",
    RDS_PASSWORD="ArtDB22swe",
    RDS_HOSTNAME="artdbinstance.czk8e1ewixri.us-east-2.rds.amazonaws.com",
    RDS_PORT=5432,
    RDS_DB_NAME="artdbinstance",
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
