import pandas as pd
import re
import os
import numpy as np

# Constants for cleaned data files
SOURCE_FILES_DIR = "/scraping/source_files/"
CLEANED_FILES_DIR = "src/"
CLEANED_ARTISTS_FILENAME = "valid_artists.json"
CLEANED_ARTWORKS_FILENAME = "valid_artworks.json"
CLEANED_MUSEUMS_FILENAME = "valid_museums.json"

# Constants for filter drop-down options
DROPDOWN_FILES_DIR = "/front-end/src/"
drop_down_params = [  # (attribute name, source file, target file)
    # Artists
    (
        "birth_place",
        CLEANED_ARTISTS_FILENAME,
        "dropdown_options_artist_birth_place.csv",
    ),
    ("status", CLEANED_ARTISTS_FILENAME, "dropdown_options_artist_status.csv"),
    ("gender", CLEANED_ARTISTS_FILENAME, "dropdown_options_artist_gender.csv"),
    ("ethnicity", CLEANED_ARTISTS_FILENAME, "dropdown_options_artist_ethnicity.csv"),
    # Artworks
    (
        "country_of_origin",
        CLEANED_ARTWORKS_FILENAME,
        "dropdown_options_artwork_country_of_origin.csv",
    ),
    ("category", CLEANED_ARTWORKS_FILENAME, "dropdown_options_artwork_category.csv"),
    ("medium", CLEANED_ARTWORKS_FILENAME, "dropdown_options_artwork_medium.csv"),
    ("type", CLEANED_ARTWORKS_FILENAME, "dropdown_options_artwork_type.csv"),
    (
        "artist_gender",
        CLEANED_ARTWORKS_FILENAME,
        "dropdown_options_artwork_artist_gender.csv",
    ),
    # Museums
    ("country", CLEANED_MUSEUMS_FILENAME, "dropdown_options_museum_country.csv"),
    (
        "business_status",
        CLEANED_MUSEUMS_FILENAME,
        "dropdown_options_museum_business_status.csv",
    ),
]


def get_previous_directory():
    return "\\".join(s for s in os.getcwd().split("\\")[:-1])


artists_data = pd.read_json(
    get_previous_directory() + SOURCE_FILES_DIR + "valid_artists.json", orient="index"
)
artworks_data = pd.read_json(
    get_previous_directory() + SOURCE_FILES_DIR + "valid_artworks.json", orient="index"
)
museums_data = pd.read_json(
    get_previous_directory() + SOURCE_FILES_DIR + "valid_museums.json", orient="index"
)

# -------------------------  ARTIST UTILS  -------------------------

# FORMAT:  <City>, <Country>
def convert_birth_place(str):
    if str:
        str = re.sub("\(.*?\)", "", str)  # remove parenthesized sub-strings
        str = re.sub("\.*\ ,", ",", str)  # condense spaces between commas
    return str


def convert_ethnicity(str):
    if str:
        str = str.split(",")[0]  # remove extra information (supposedly comes after ',')
    return str


def create_cleaned_artists_src():
    new_df = artists_data.copy()
    new_df.rename(
        columns={col: col.replace("-", "_") for col in new_df.columns}, inplace=True
    )
    new_df.replace("<NA>", np.nan, inplace=True)

    new_df["death_date"] = new_df["death_date"].astype(
        "Int64"
    )  # apparently, Pandas assumes the datatype should be float64. must undo this conversion.
    new_df["birth_place"] = new_df["birth_place"].map(convert_birth_place)
    new_df["ethnicity"] = new_df["ethnicity"].map(convert_ethnicity)
    new_df.to_json(
        os.getcwd() + "/" + CLEANED_FILES_DIR + CLEANED_ARTISTS_FILENAME,
        orient="index",
        indent=4,
    )


# -------------------------  ARTWORK UTILS  -------------------------

# INPUT:  an artwork country_of_origin
def extract_country_and_period(str):
    mat = re.match("(\D*),(\D*\d+th.*)", str)  # 'Xth century' checker
    if mat:
        groups = mat.groups()
        return (groups[0], groups[1].lstrip())
    mat = re.match(
        "(.*),(.*(period|dynasty).*)", str
    )  # contains 'period' or 'dynasty' checker
    if mat:
        groups = mat.groups()
        return (groups[0], groups[1].lstrip())
    return (str, None)


def create_cleaned_artworks_src():
    new_df = artworks_data.copy()
    new_df.rename(
        columns={col: col.replace("-", "_") for col in new_df.columns}, inplace=True
    )
    new_df.replace("Unknown", np.nan, inplace=True)

    # more annoying Pandas conversions to float64. must undo them all.
    new_df["artist_id"] = new_df["artist_id"].astype("Int64")
    new_df["owning_museum_id"] = new_df["owning_museum_id"].astype("Int64")

    countries_and_periods = [
        extract_country_and_period(s) for s in new_df["country_of_origin"]
    ]
    new_df["country_of_origin"] = [cp[0] for cp in countries_and_periods]
    new_df["period"] = [cp[1] for cp in countries_and_periods]  # New column
    new_df.to_json(
        os.getcwd() + "/" + CLEANED_FILES_DIR + CLEANED_ARTWORKS_FILENAME,
        orient="index",
        indent=4,
    )


# -------------------------  MUSEUM UTILS  -------------------------


def create_cleaned_museums_src():
    new_df = museums_data.copy()
    new_df.rename(
        columns={col: col.replace("-", "_") for col in new_df.columns}, inplace=True
    )
    new_df.replace("NULL", np.nan, inplace=True)
    new_df.to_json(
        os.getcwd() + "/" + CLEANED_FILES_DIR + CLEANED_MUSEUMS_FILENAME,
        orient="index",
        indent=4,
    )


# -------------------------  DROP_DOWN VALUES UTILS  -------------------------


def write_dropdown_values():
    for param in drop_down_params:
        df = pd.read_json(CLEANED_FILES_DIR + param[1], orient="index")
        with open(
            get_previous_directory() + DROPDOWN_FILES_DIR + param[2], "w"
        ) as file:
            file.write(",".join(["'{t}'".format(t=s) for s in df[param[0]].unique()]))


if __name__ == "__main__":
    create_cleaned_artists_src()
    create_cleaned_artworks_src()
    create_cleaned_museums_src()
    write_dropdown_values()
