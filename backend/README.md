# Directions for Developing on Backend

### First Time Setup Instructions

1. Install Docker Desktop

1. Pull the krent2502/idb-beanstalk-backend Docker image with the command:

    `docker pull krent2502/idb-beanstalk-backend`

### Recurring Setup Instructions
1. Open Docker Desktop. If you do not have Docker Desktop running, docker will present the error message:

    `docker: error during connect: This error may indicate that the docker daemon is not running`

    when you try to start the container.

1. Start the krent2502/idb-beanstalk-backend image.
    - If you are using Windows, this can be done in the _command prompt_ terminal with the command:

        `docker run --rm -it -p 5000:5000 -v %cd%:/usr/python -w /usr/python krent2502/idb-beanstalk-backend`

    - If you are using Windows Powershell, use the command:

        `docker run --rm -it -p 5000:5000 -v ${PWD}:/usr/python -w /usr/python krent2502/idb-beanstalk-backend`
    
    - In general, the command should be:

        `docker run --rm -it -p 5000:5000 -v <current_working_directory>:/usr/python -w /usr/python krent2502/idb-beanstalk-backend`

1. Run the Flask app to see how your code changes will affect data returned by the ArtDB API with the command: 

    `make flask`

    After flask activates, you can go to localhost:5000/ to test the various API endpoints.

1. Run Python unit tests with the command:
    
    `make python-tests`

1. When you are ready to deploy code changes to the server, zip up your backend source code with the command:

    `make backend-zip`


### Deploying to Elastic Beanstalk
1. Log in to the AWS console as an IAM user.

1. Navigate to the Elastic Beanstalk page.

1. Click into the `ArtDB-beanstalk-env` environment.

1. Click "Upload and Deploy"

1. Upload the src.zip that was created with the `make backend-zip` command.

    - Note: The "Version label" field will be automatically populated when you upload your file. **DO NOT** change this value. Changing this value can cause the deployment to fail.

1. Click "Deploy". Beanstalk will start deploying the code. This can take a few minutes.

1. After Beanstalk finishes deploying, make sure that there is a green check under "Health". If there isn't, debug the issue and redeploy to get Beanstalk back to a healthy state.

1. Congrats! Your changes have been deployed. Remember to also push your code changes to GitLab and to visit the API endpoints to ensure everything behaves as you expect.
