from models import db, Artist, Artwork, Museum, Exhibition
from flask_sqlalchemy import inspect
import json
import re

"""
Initialize the database (drop existing data and re-fill it)
"""

ARTISTS_SOURCE = "src/valid_artists.json"
ARTWORKS_SOURCE = "src/valid_artworks.json"
MUSEUMS_SOURCE = "src/valid_museums.json"
EXHIBITIONS_SOURCE = "src/exhibitions.json"


def is_integer(model, key):
    return type(model.__getattribute__(model, key).type) == db.Integer


# The following functions return lists of Model instances
def load_artists():
    data = json.load(open(ARTISTS_SOURCE))
    result = []
    for id in sorted(data.keys()):
        rst = {}
        for k, v in data[id].items():
            if k != "works":
                if v and is_integer(Artist, k):
                    if not type(v) == int:
                        v = int(re.search("([-]*\d+)", v).groups()[0])
                rst[k] = v
        rst["id"] = id
        result.append(Artist(**rst))
    return result


def load_artworks():
    data = json.load(open(ARTWORKS_SOURCE))
    result = []
    for id in sorted(data.keys()):
        rst = {}
        for k, v in data[id].items():
            if k != "exhibition_history":
                if v and is_integer(Artwork, k):
                    if not type(v) == int:
                        v = int(re.search("([-]*\d+)", v).groups()[0])
                rst[k] = v
        rst["id"] = id
        result.append(Artwork(**rst))
    return result


def load_museums():
    data = json.load(open(MUSEUMS_SOURCE))
    result = []
    for id in sorted(data.keys()):
        rst = {}
        for k, v in data[id].items():
            if v and is_integer(Museum, k):
                if not type(v) == int:
                    v = int(re.search("([-]*\d+)", v).groups()[0])
            rst[k] = v
        rst["id"] = id
        result.append(Museum(**rst))
    return result


def load_exhibitions():
    data = json.load(open(EXHIBITIONS_SOURCE))
    result = []
    for id in sorted(data.keys()):
        rst = data[id]
        rst["id"] = id
        result.append(Exhibition(**rst))
    return result


if __name__ == "__main__":
    engine = db.get_engine()

    # Drop existing tables
    if inspect(engine).has_table(Artist.__tablename__):
        Artist.__table__.drop(engine)
    if inspect(engine).has_table(Artwork.__tablename__):
        Artwork.__table__.drop(engine)
    if inspect(engine).has_table(Museum.__tablename__):
        Museum.__table__.drop(engine)
    if inspect(engine).has_table(Exhibition.__tablename__):
        Exhibition.__table__.drop(engine)

    # Re-initialize existing tables
    Artist.__table__.create(engine)
    Artwork.__table__.create(engine)
    Museum.__table__.create(engine)
    Exhibition.__table__.create(engine)

    # Re-populate tables
    db.session.add_all(load_artists())
    db.session.add_all(load_artworks())
    db.session.add_all(load_museums())
    db.session.add_all(load_exhibitions())
    db.session.commit()
