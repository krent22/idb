from distutils.util import execute
from flask import jsonify, request
from sqlalchemy import select, and_, or_, cast, String, nullslast, func, case
from models import app, db, Artist, Artwork, Museum, Exhibition
import pandas as pd
from convert import (
    convert_museum_to_json,
    convert_artwork_to_json,
    convert_artist_to_json,
    convert_exhibition_to_json,
)
from sqlalchemy import Integer
from sqlalchemy.sql.expression import desc
from helpers import search, sort, filter
import requests, json
import numpy as np

# import awsgi


# def handler(event, context):
#     return awsgi.response(app, event, context)


@app.route("/")
def hello_world():
    return jsonify("Welcome to the ArtDB API!")


@app.route("/artists", methods=["GET"])
def get_artists():
    params = request.args.to_dict()

    exec_stmt = select(Artist)

    exec_stmt = filter(exec_stmt, Artist, params)

    terms = {}
    if "query" in params:
        terms = params["query"].split()
        sort_relevance = "sort" not in params

        subq_artist_id = (
            select(Artwork.artist_id)
            .filter(
                or_(
                    or_(Artwork.owning_museum.ilike(f"%{ term }%") for term in terms),
                    or_(Artwork.name.ilike(f"%{ term }%") for term in terms),
                )
            )
            .distinct()
        )

        exec_stmt = search(exec_stmt, Artist, terms, sort_relevance, subq_artist_id)

    if "sort" in params:
        exec_stmt = sort(exec_stmt, Artist, params["sort"])

    if "sort" not in params and "query" not in params:
        exec_stmt = exec_stmt.order_by(Artist.name)

    pag = db.paginate(exec_stmt)
    rst = pag.items

    # For visualization (artist gender vs. #artworks) bar graph
    vis_data = None
    if "vis_data" in params:
        vis_rst = db.session.execute(
            select([Artist.gender, func.sum(Artist.number_of_works)])
            .filter(Artist.gender != None)
            .group_by(Artist.gender)
        )
        vis_rst = [i for i in vis_rst]
        vis_data = {
            "gender": [i[0] for i in vis_rst],
            "#artworks": [i[1] for i in vis_rst],
        }

    rst_list = []
    for person in rst:
        # query exhibition history for this artist
        rst_list.append(convert_artist_to_json(person, False))
    rst_dict = {}
    rst_dict["total"] = pag.total
    rst_dict["start"] = pag.first
    rst_dict["data"] = rst_list
    rst_dict["highlight"] = terms
    rst_dict["vis_data"] = vis_data
    return rst_dict


@app.route("/artists/<id>", methods=["GET"])
def get_artist_by_id(id):
    rst = Artist.query.get(id)
    # query exhibition history for this artist
    rst_dict = {}
    rst_dict["data"] = convert_artist_to_json(rst, True)
    return rst_dict


@app.route("/artworks", methods=["GET"])
def get_artworks():
    params = request.args.to_dict()

    exec_stmt = select(Artwork)

    exec_stmt = filter(exec_stmt, Artwork, params)

    terms = {}
    if "query" in params:
        terms = params["query"].split()
        sort_relevance = "sort" not in params

        subq_mus_id = select(Museum.id).filter(
            or_(Museum.name.ilike(f"%{ term }%") for term in terms)
        )
        subq_artwork_id = select(Exhibition.artwork_id).filter(
            or_(
                or_(Exhibition.exhibition_date.ilike(f"%{ term }%") for term in terms),
                Exhibition.museum_id.in_(subq_mus_id),
            )
        )

        exec_stmt = search(exec_stmt, Artwork, terms, sort_relevance, subq_artwork_id)

    if "sort" in params:
        exec_stmt = sort(exec_stmt, Artwork, params["sort"])

    if "sort" not in params and "query" not in params:
        exec_stmt = exec_stmt.order_by(
            nullslast(case((Artwork.image_url != None, Artwork.name), else_=(None)))
        )

    pag = db.paginate(exec_stmt)
    rst = pag.items

    # For visualization (year vs. #artworks) line graph
    vis_data = None
    if "vis_data" in params:
        vis_rst = db.session.execute(
            select(Artwork.date_of_creation).filter(Artwork.date_of_creation != None)
        )
        data = sorted([i[0] for i in vis_rst])
        BINS = [i for i in range(0, 2001, 50)]
        bins = pd.cut(data, BINS).value_counts()
        sorted_data = sorted([i for i in bins.index], key=lambda x: x.left)
        x_data = [i.left for i in sorted_data]
        y_data = [bins[i] for i in sorted_data]
        vis_data = {
            "year": [int(i) for i in x_data],
            "#artworks": [int(i) for i in y_data],
        }

    rst_list = []
    for art in rst:
        # query exhibition history for this artwork
        rst_list.append(convert_artwork_to_json(art, False))
    rst_dict = {}
    rst_dict["total"] = pag.total
    rst_dict["start"] = pag.first
    rst_dict["data"] = rst_list
    rst_dict["highlight"] = terms
    rst_dict["vis_data"] = vis_data

    return rst_dict


@app.route("/artworks/<id>", methods=["GET"])
def get_artwork_by_id(id):
    rst = Artwork.query.get(id)
    # query exhibition history for this artwork
    rst_dict = {}
    rst_dict["data"] = convert_artwork_to_json(rst, True)
    return rst_dict


@app.route("/museums", methods=["GET"])
def get_museums():
    params = request.args.to_dict()

    exec_stmt = select(Museum)

    exec_stmt = filter(exec_stmt, Museum, params)

    terms = {}
    if "query" in params:
        terms = params["query"].split()
        sort_relevance = "sort" not in params

        subq_artwork_id = select(Artwork.id).filter(
            or_(Artwork.name.ilike(f"%{ term }%") for term in terms)
        )
        subq_artist_id = select(Artist.id).filter(
            or_(Artist.name.ilike(f"%{ term }%") for term in terms)
        )
        subq_museum_id = select(Exhibition.museum_id).filter(
            or_(
                Exhibition.artwork_id.in_(subq_artwork_id),
                Exhibition.artist_id.in_(subq_artist_id),
            )
        )

        exec_stmt = search(exec_stmt, Museum, terms, sort_relevance, subq_museum_id)

    if "sort" in params:
        exec_stmt = sort(exec_stmt, Museum, params["sort"])

    if "sort" not in params and "query" not in params:
        exec_stmt = exec_stmt.order_by(Museum.name)

    pag = db.paginate(exec_stmt)
    rst = pag.items

    # For visualization (country vs. #museums) bar graph
    vis_data = None
    if "vis_data" in params:
        vis_rst = db.session.execute(
            select([Museum.country, func.count(Museum.name)])
            .filter(Museum.country != None)
            .group_by(Museum.country)
        )
        vis_rst = [i for i in vis_rst]
        vis_data = {
            "country": [i[0] for i in vis_rst],
            "#museums": [i[1] for i in vis_rst],
        }

    rst_list = []
    for m in rst:
        rst_list.append(convert_museum_to_json(m, False))
    rst_dict = {}
    rst_dict["total"] = pag.total
    rst_dict["start"] = pag.first
    rst_dict["data"] = rst_list
    rst_dict["highlight"] = terms
    rst_dict["vis_data"] = vis_data
    return rst_dict


@app.route("/museums/<id>", methods=["GET"])
def get_museum_by_id(id):
    return {"data": convert_museum_to_json(Museum.query.get(id), True)}


@app.route("/exhibitions", methods=["GET"])
def get_exhibitions():
    rst = db.session.query(Exhibition).all()
    rst_list = []
    for ex in rst:
        rst_list.append(convert_exhibition_to_json(ex))
    rst_dict = {}
    rst_dict["total"] = rst.total
    rst_dict["start"] = 0
    rst_dict["data"] = rst_list
    return rst_dict


@app.route("/developer_viz", methods=["GET"])
def get_developer_visualization_data():
    ing_data = pd.DataFrame.from_dict(
        json.loads(requests.get("https://api.mealmaker.me/ingredients").text)["page"]
    )
    rec_data = pd.DataFrame.from_dict(
        json.loads(requests.get("https://api.mealmaker.me/recipes").text)["page"]
    )
    stores_data = pd.DataFrame.from_dict(
        json.loads(requests.get("https://api.mealmaker.me/stores").text)["page"]
    )

    # FOR VISUALIZATION 1:  Bar graph for frequency of caution labels (from Ingredients)
    data = np.unique(np.concatenate(ing_data["cautions"]), return_counts=True)
    caution_labels = {"caution": list(data[0]), "frequency": data[1].tolist()}

    # FOR VISUALIZATION 2:  Histogram for cook time (from Recipes)
    cook_times = {"cook_times": list(rec_data["cook_time"].dropna().values)}

    # FOR VISUALIZATION 3:  Map of Texas for frequency of grocery stores in each city (from Stores)
    data = stores_data["city"].value_counts()
    cities = {"city": list(data.index), "frequency": data.values.tolist()}

    return {
        "ingredient_viz": caution_labels,
        "recipe_viz": cook_times,
        "store_viz": cities,
    }


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
