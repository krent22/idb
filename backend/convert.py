from models import db, Artist, Artwork, Museum, Exhibition
from sqlalchemy import select


def convert_museum_to_json(m: Museum, all_info: bool):
    museum_dict = {}
    museum_dict["id"] = m.id
    museum_dict["name"] = m.name
    museum_dict["country"] = m.country
    museum_dict["visitors_per_year"] = m.visitors_per_year
    museum_dict["date_established"] = m.date_established
    museum_dict["founder"] = m.founder
    museum_dict["director"] = m.director
    museum_dict["collection_size"] = m.collection_size
    museum_dict["phone_number"] = m.phone_number
    museum_dict["address"] = m.address
    museum_dict["website"] = m.website
    museum_dict["business_status"] = m.business_status
    museum_dict["rating"] = m.rating
    museum_dict["latitude"] = m.latitude
    museum_dict["longitude"] = m.longitude
    museum_dict["utc_offset"] = m.utc_offset
    museum_dict["logo"] = m.logo
    museum_dict["image_exterior"] = m.image_exterior
    museum_dict["image_interior"] = m.image_interior

    if all_info:
        query = (
            select(
                Exhibition.artist_id,
                Exhibition.artwork_id,
                Exhibition.exhibition_date,
                Artist.name,
                Artwork.name,
                Artwork.image_url,
            )
            .select_from(Museum)
            .join(Exhibition, Exhibition.museum_id == m.id)
            .join(Artist, Exhibition.artist_id == Artist.id)
            .join(Artwork, Exhibition.artwork_id == Artwork.id)
            .distinct(Exhibition.artist_id, Exhibition.artwork_id)
        )
        rst = db.session.execute(query)
        exhib_history = []
        for row in rst:
            exhib_dict = {}
            exhib_dict["artist_id"] = row[0]
            exhib_dict["artwork_id"] = row[1]
            exhib_dict["exhibition_date"] = row[2]
            exhib_dict["artist_name"] = row[3]
            exhib_dict["artwork_name"] = row[4]
            exhib_dict["artwork_image_url"] = row[5]
            exhib_history.append(exhib_dict)
        museum_dict["exhibition_history"] = exhib_history

    return museum_dict


def convert_artwork_to_json(art: Artwork, all_info: bool):
    artwork_dict = {}
    artwork_dict["id"] = art.id
    artwork_dict["name"] = art.name
    artwork_dict["country_of_origin"] = art.country_of_origin
    artwork_dict["date_of_creation"] = art.date_of_creation
    artwork_dict["period"] = art.period
    artwork_dict["category"] = art.category
    artwork_dict["type"] = art.type
    artwork_dict["artist_name"] = art.artist_name
    artwork_dict["artist_id"] = art.artist_id
    artwork_dict["artist_gender"] = art.artist_gender
    artwork_dict["owning_museum"] = art.owning_museum
    artwork_dict["owning_museum_id"] = art.owning_museum_id
    artwork_dict["location"] = art.location
    artwork_dict["dimensions"] = art.dimensions
    artwork_dict["acquisition"] = art.acquisition
    artwork_dict["medium"] = art.medium
    artwork_dict["description"] = art.description
    artwork_dict["image_url"] = art.image_url

    if all_info:
        query = (
            select(Museum.name, Exhibition.exhibition_date, Exhibition.museum_id)
            .select_from(Artwork)
            .join(Exhibition, Exhibition.artwork_id == Artwork.id)
            .join(Museum, Exhibition.museum_id == Museum.id)
            .where(Artwork.id == art.id)
        )
        exhib_history = []
        for row in db.session.execute(query):
            exhib_dict = {}
            exhib_dict["museum_name"] = row[0]
            exhib_dict["exhibition_date"] = row[1]
            exhib_dict["museum_id"] = row[2]
            exhib_history.append(exhib_dict)
        artwork_dict["exhibition_history"] = exhib_history

    return artwork_dict


def convert_artist_to_json(person: Artist, all_info: bool):
    artist_dict = {}
    artist_dict["id"] = person.id
    artist_dict["name"] = person.name
    artist_dict["gender"] = person.gender
    artist_dict["biography"] = person.biography
    artist_dict["ethnicity"] = person.ethnicity
    artist_dict["birth_date"] = person.birth_date
    artist_dict["death_date"] = person.death_date
    artist_dict["status"] = person.status
    artist_dict["number_of_works"] = person.number_of_works
    artist_dict["other_names"] = person.other_names
    artist_dict["birth_place"] = person.birth_place
    artist_dict["image"] = person.image

    if all_info:
        query = select(
            Artwork.id,
            Artwork.name,
            Artwork.image_url,
            Artwork.owning_museum,
            Artwork.owning_museum_id,
        ).where(Artwork.artist_id == person.id)
        rst = []
        for row in db.session.execute(query):
            art_dict = {}
            art_dict["artwork_id"] = row.id
            art_dict["artwork_name"] = row.name
            art_dict["artwork_image"] = row.image_url
            art_dict["owning_museum"] = row.owning_museum
            art_dict["owning_museum_id"] = row.owning_museum_id
            rst.append(art_dict)
        artist_dict["works"] = rst

    return artist_dict


def convert_exhibition_to_json(ex: Exhibition):
    ex.__delattr__("_sa_instance_state")
    return ex.__dict__
