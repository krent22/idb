# ArtDB
## Names, EIDs, and Gitlab IDs
| Name                 | UTEID    | GitLab ID          |
| -------------------- | -------  | ------------------ |
| Polo Lopez           | ll34552  | @Polo_Lopez        |
| Don Nguyen           | dhn384   | @TechDonut         |
| Joseph Martinez      | jtm4237  | @jtmartinez        |
| Joel Setiawan Chong  | jas24943 | @JoelSetiawan      |
| Karen Tseng          | kt25977  | @krent22           |

## Git SHA
Phase 1: aa412e5ba436b9c5f84ca946a9b500c03f2a1583

Phase 2: f35d89f82438443eb549e68f3c1668f6740588f1

Phase 3: 7091eb61bcc688b8a2039b0e9d24d18cbac85155

Phase 4: 09897146235c826a325b1ea06b8be70770a7fd2b

## Project Leader
Phase 1: Don Nguyen (dhn384)

Phase 2: Karen Tseng (kt25977)

Phase 3: Polo Lopez (ll34552)

Phase 4: Joel Setiawan Chong (jas24943)

## Link to CI/CD Pipeline
[Pipeline](https://gitlab.com/krent22/idb/-/pipelines)

## Link to Website
[ArtDB](https://www.artdb.me)

## Link to API Documentation
[Artdb API Documentation](https://documenter.getpostman.com/view/23525299/2s83tDpXqa)

## Completion Time / Member
### Phase 1
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Polo Lopez           | 10        | 14     |
| Don Nguyen           | 12        | 16     |
| Joseph Martinez      | 15        | 12     |
| Joel Setiawan Chong  | 15        | 16     |
| Karen Tseng          | 30        | 25     |

### Phase 2
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Polo Lopez           | 10        | 17     |
| Don Nguyen           | 20        | 12     |
| Joseph Martinez      | 15        | 22     |
| Joel Setiawan Chong  | 15        | 12     |
| Karen Tseng          | 25        | 19     |

### Phase 3
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Polo Lopez           | 10        | 18     |
| Don Nguyen           | 15        | 17     |
| Joseph Martinez      | 20        | 18     |
| Joel Setiawan Chong  | 20        | 22     |
| Karen Tseng          | 20        | 17     |

### Phase 4
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Polo Lopez           | 8         | 10     |
| Don Nguyen           | 10        | 8      |
| Joseph Martinez      | 10        | 14     |
| Joel Setiawan Chong  | 20        | 20     |
| Karen Tseng          | 10        | 18     |


## Comments
This repository contains code that is based off of previous projects or documentation sites.
- Our .gitlab-ci.yml file is from the TexasVotes repository linked in the Phase I rubric.
- Our Makefile is from the cs373-sustainability repository linked in the Phase I rubric.
- The .eslintignore file is from the GetThatBread repository.
- The instance page formatting is also heavily based off of code from the GetThatBread repository.
- The README page formatting is also heavily based off of code from the GetThatBread repository
- The About page and CSS is based off of the AnimalWatch repository.
- Code for displaying maps were derived from the UniverCity repository and the sample code on Google Map’s API documentation.
- Code for tables and carousels are derived from React Bootstrap’s documentation pages.
- The Splash page and CSS is based off of the AnimalWatch repository.
- The backend config files and gitlab-ci.yml files, as well as some of the Python scripts (‘app.py’, ‘init_db.py’, and ‘models.py’) are based on the [GetThatBread repository](https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/tree/main/backend)
- The selenium tests are also based on the [GetThatBread repository](https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/tree/main/frontend/getthatbread/selenium)[GalleryDB repository](https://gitlab.com/JoeMuff999/gallerydb/-/blob/main/front-end/gui_tests/searchTests.py)
- The jest tests are based off of the [GalleryDB repository]( https://gitlab.com/JoeMuff999/gallerydb/-/blob/main/front-end/src/components/AllJest.test.js )
- The unit tests are based off of the [GalleryDB repository](https://gitlab.com/JoeMuff999/gallerydb/-/blob/main/back-end/tests.py)
- Highlighting based off of GalleryDB and Seastainable: https://gitlab.com/maramanskie/seastainable/-/blob/main/frontend/src/pages/GlobalSearch.jsx
https://gitlab.com/JoeMuff999/gallerydb/-/blob/main/front-end/src/components/Search.js
- Search bar code based off of: https://dev.to/salehmubashar/search-bar-in-react-js-545l 
- Priority in searches based off of: https://gitlab.com/maxwthomas/cs373-idb/-/blob/main/backend/app.py 
- Logic to check for 0’s in an array in javascript based off of: https://www.tutorialspoint.com/counting-unique-elements-in-an-array-in-javascript 
- Code to modify color of search bar based off of: https://stackoverflow.com/questions/46966413/how-to-style-material-ui-textfield 
- Code to create MuseumMap is based off of: https://codesandbox.io/s/choropleth-mapchart-showing-vulnerability-by-country-euuik?from-embed=&file=/src/MapChart.js
- TopoJSON for TexasCityStoreFreqMap is taken from and TexasCityStoreFreqMap inspired by: https://github.com/deldersveld/topojson/blob/master/countries/us-states/TX-48-texas-counties.json and https://codesandbox.io/s/basic-markers-3fn8g?from-embed=&file=/src/MapChart.js
BarCharts and LineCharts code modified from: https://gitlab.com/maramanskie/seastainable/-/blob/main/frontend/src/pages/Visualizations/Charts/BarChart.js
